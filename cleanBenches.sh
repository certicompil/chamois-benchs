#!/usr/bin/env bash

#------------------------------------------ Usage and parameter
# usage: cleanBenches [target]? [FILE]...
# if no target applyed, then cleaning binaries, object, asm ...
# to clean only .csv .out, call with 'csvclean' as target
#
#-----------------------------


#------------------------------------------ begin
#

scrptnm=$(basename $0)
# echo "scrptnm='$scrptnm', arg1='$1'"

#
# ------------ 1st:  echo "parse $# input parameters"
# if $1 not defined -> binclean target
if [ "$1" == "csvclean" ]; then
  target=$1
else
  target=binclean
fi
# target=csvclean
# echo "$target"

source com/preliminary.sh
# get VERIBENCH_ROOT, listAbsPath2Bench, preGLOBAL_ERROR_LOG_FILE, preNbInvalidPath


rm -f $VERIBENCH_ROOT/com/$VERIBENCH/$VERIBENCH.o


for bench in $listAbsPath2Bench; do
  util.echoC "Cleaning (target='$target') $bench..." $yellow
  (cd $bench && make -s $target)
done

pre.traceInvalidPath

if [ $preNbInvalidPath -ne 0 ]; then
  util.echoC "bad path and/or build failed: ($GLOBAL_ERROR_LOG_FILE)" $red
  cat $GLOBAL_ERROR_LOG_FILE
fi

# ----------------------------------------- eof cleanBenches.sh
#_____
