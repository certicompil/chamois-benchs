SHELL=/bin/bash

# -------------------------------   input parameters
# $(CC), $(ARCH_AS), $(ARCH_CC), $(CLK_CFLAG) $(PREFIX), $(CFLAGS), 
# $(VERIBENCH_ROOT), $(RUN_CMD), $(KVX_EXECFILE), $(KVX_ARGS), $(IS_KVX)



# -------------------- assignments for each specific benches
# The ?= operator is an extension that behaves like =, except
# that the assignment only occurs if the variable is not already set.
# build: $(TARGET), $(ALL_CFILES), $(LOCAL_CFLAGS)
# run: $(EXECUTE_ARGS), $(STDIN), $(STDOUT)

# TOREMOVE Default value for RUN_CMD  taskset -c 0
# RUN_CMD ?= taskset -c 0


# Default name of the target 
TARGET ?= unnamed

# list of c files
ALL_CFILES ?= $(wildcard *.c)

# Benches specific CFLAGS 
override CFLAGS += $(LOCAL_CFLAGS)


# Redirections of stdin and stdout
# (including the < or >, for instance `STDIN=< file.txt`)
# STDIN ?=
# STDOUT ?=

# Default value for the arguments of execution
# EXECUTE_ARGS ?= 

# c99 breaks certain benchmarks that expect GNUC
# §§§§ TODO do we still need this ? 
C99 ?= -std=gnu99 

# Set SILENT to something to silence all run outputs
ifdef $(SILENT)
SILENT=> /dev/null
endif


# ----------------------------------------   build

# --------------------   veribench.o

VERIBENCHF=$(VERIBENCH_ROOT)/com/veribench
VERIBENCH=$(VERIBENCHF)/veribench
CLK_CFLAG ?= -O3

.SECONDARY:
$(VERIBENCH).o: $(VERIBENCH).c
	$(ARCH_CC) $(CLK_CFLAG)  $< -c -o $@

# --------------------   rules

asm/%.$(PREFIX).s: %.c
	@mkdir -p $(@D)
	$(CC) -I$(VERIBENCHF) $(CFLAGS) -S $< -o $@

obj/%.$(PREFIX).o: asm/%.$(PREFIX).s
	@mkdir -p $(@D)
	$(ARCH_AS) $< -o $@

bin/$(TARGET).$(PREFIX).bin: $(addprefix obj/,$(ALL_CFILES:.c=.$(PREFIX).o)) $(VERIBENCH).o
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $+ $(LIBS) -lm -o $@

binbuild: bin/$(TARGET).$(PREFIX).bin


# ----------------------------------------   run

# Text editor parsers do not like this very much, hence the else branch
ifdef IS_KVX
KVX_QUOTES := "
else
NO_QUOTES := "
endif
out/%.out: bin/%.bin
	@mkdir -p $(@D)
	$(RUN_CMD) $(KVX_EXECFILE)$< $(KVX_ARGS)$(KVX_QUOTES)$(subst __BASE__,$(patsubst %.out,%,$@),$(EXECUTE_ARGS))$(KVX_QUOTES) $(STDIN) $(STDOUT)  2> $@ | tee -a $@ > /dev/null

# | tee -a $@ $(SILENT)


# ----------------------------------------   clean
binclean:
	rm -f *.o *.s *.bin *.out *.log buildme.sh
	rm -f *.csv
	rm -rf asm/ bin/ obj/ out/
	rm -f ../*.o
	rm -f $(VERIBENCH).o

csvclean:
	rm -f out/*.out
	rm -f measures*.csv


# ----------------------------------------   test 
ttest:
ifdef CC
	@echo 'CC is defined:' $(CC)
else
	@echo 'no CC around'
endif
ifdef ARCH_CC
	@echo 'ARCH_CC is defined:' $(ARCH_CC)
else
	@echo 'no ARCH_CC around'
endif
ifdef ARCH_AS
	@echo 'ARCH_AS is defined:' $(ARCH_AS)
else
	@echo 'no ARCH_AS around'
endif
ifdef PREFIX
	@echo 'PREFIX is defined:' $(PREFIX)
else
	@echo 'no PREFIX around'
endif
ifdef CFLAGS
	@echo 'CFLAGS is defined:' $(CFLAGS)
else
	@echo 'no CFLAGS around'
endif
ifdef VERIBENCH_ROOT
	@echo 'VERIBENCH_ROOT is defined:' $(VERIBENCH_ROOT)
else
	@echo 'no VERIBENCH_ROOT around'
endif
ifdef RUN_CMD
	@echo 'RUN_CMD is defined:' $(RUN_CMD)
else
	@echo 'no RUN_CMD around'
endif
ifdef KVX_EXECFILE
	@echo 'KVX_EXECFILE is defined:' $(KVX_EXECFILE)
else
	@echo 'no KVX_EXECFILE around'
endif
ifdef KVX_ARGS
	@echo 'KVX_ARGS is defined:' $(KVX_ARGS)
else
	@echo 'no KVX_ARGS around'
endif
