#!/usr/bin/env bash

# -----------------------------------------
# usage: buildBin.sh file.json [FILE]...
# - file.json describe different configurations: compiler and options
# - FILE is either a relative path to a bench directory OR
# a text file with a list of relative paths to a benches directory (VERIBENCH_BENCHESF)
# (this can be defined with cde as 'export VERIBENCH_BENCHESF="/home/toto/my/benches')
# the default benches directory is defined in com/preliminary.sh as $VERIBENCH_ROOT/$DEFAULT_BENCHESF

#
# DEPENDANCE:
# jq :  command-line JSON processor,
# it is written in portable C, and it has zero runtime dependencies
#  - installation see @ https://github.com/stedolan/jq/wiki/Installation


# principle:
# - VERIBENCH_ROOT is the root of this tool, where:
#    + `benches` the default main directory of benches
#    + `com` common ressources (veribench.o .h .c ...), scripts
#    + `conf` directory for configuration files in json format
# - The root path for bench is defined by default relative to VERIBENCH_ROOT,
#   or via an environment variable VERIBENCH_BENCHESF
# For each path there is a specific bench directory
# For each bench in  do
#  cd bench, and call make with CC/option from <conf_file>.json
#  each local Makefile defined some variable, then include rules.mk
#  compilation's error are log in $BuildCommandsErrfile in each bench directory



# Please do not use this information or algorithm in any way that might
# upset the balance of the universe or otherwise cause n qvfgheonapr va
# gur fcnpr-gvzr pbagvahhz. arf =P

# Enable Debugging with “set -x”, Disable Debugging with “set +x”
# set -x


#
#--------------------------------------------------------




#--------------------------------------------------------
#
scrptnm=$(basename $0)
# echo "scrptnm='$scrptnm', arg1='$1'"


#--------------------------------------------------------- usage and parameter
#
# check at least 1 argument (json configuration file)
#
if [ "$#" -lt 1 ]; then
  echo "Usage: $scrptnm <config.json> [FILE]..." >&2
  exit 1
elif  ! [ -e "$1" ]; then
  echo  "$scrptnm: cannot access '$1': No such file or directory" >&2
elif  [ -d "$1" ]; then
  echo  "$scrptnm: '$1': Is a directory" >&2
elif  ! [ -s "$1" ]; then
  echo "$scrptnm: '$1': Is a zero size file" >&2
  exit 1
fi
# JSON_INPUT is non zero size file
JSON_INPUT=$1
shift



#--------------------------------------------------------- common preliminary
#
source com/preliminary.sh
# definitions some utilities (echoC ...)
# VERIBENCH_ROOT, VERIBENCH_BENCHESF, listAbsPath2Bench

if [ -z "$listAbsPath2Bench" ]; then
  pre.errmsg "[BUILD] nothing to build."
  exit 1
fi

# the list of paths selected
BENCH_LIST=$listAbsPath2Bench

# number of recipes (jobs) to run simultaneously.
MAKEJOBS=-j8

MAKETARGET=binbuild
# MAKETARGET=ttest

readonly DFLT_CLOCK_CFLAG=" -O3"

# format output echo
readonly SP="#"

# backup file suffix
readonly OUTBackMark="~"

# infoFile
readonly infoOutFile="infoBuild.txt"


#-------------------------------------------------------- trace infoOutFile
#
# info file ( to get a trace of os version, compiler version, json conf, ..."
readonly preInf="#----------------------------------------------- "
if [ -e $infoOutFile ]; then
  mv $infoOutFile $infoOutFile$OUTBackMark
fi
> $infoOutFile
echo "${preInf}date:$(date +%Y%m%d%H%M%S)" >> $infoOutFile
echo "$(date) "  >> $infoOutFile
echo "${preInf}uname -a" >> $infoOutFile
echo "$(uname -a)" >>  $infoOutFile
util.getGitInfo
if [ $? == 0 ]; then
  echo "${preInf}git info" >> $infoOutFile
  echo $(git config --get remote.origin.url 2>/dev/null) >> $infoOutFile
  echo "GIT_BRANCH=${GIT_BRANCH}, GIT_REV=${GIT_REV}${GIT_DIFF}, GIT_TAG=${GIT_TAG}" >> $infoOutFile
fi



#------------------------------------------------------ get paramaters from json
#
util.echoC "building binaries with conf $JSON_INPUT" $cyan
# util.trace "(in $scrptnm:$LINENO) BENCHE_LIST=($BENCH_LIST)"

# get json global CFLAG:
CFLAGGLOB=$(jq '.cFlag' $JSON_INPUT | tr -d '"')

# get json global archAS (value is passed to rules.mk via shell environment) :
tt=$(jq '.archAS' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
  export ARCH_AS="${tt}"
fi

# get json global archCC (value is passed to rules.mk via shell environment)
tt=$(jq '.archCC' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
  export ARCH_CC="${tt}"
fi

# get json global clockCFlag (value is passed to rules.mk via shell environment)
tt=$(jq '.clockCFlag' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
  export CLK_CFLAG="${tt}"
  # util.trace "(in $scrptnm:$LINENO) from json: CLK_CFLAG='$CLK_CFLAG'"
else
  export CLK_CFLAG=$DFLT_CLOCK_CFLAG
  # util.trace "(in $scrptnm:$LINENO) from dflt: CLK_CFLAG='$CLK_CFLAG'"
fi

# get json global runCMD (value is passed to rules.mk via shell environment):
tt=$(jq '.runCMD' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
  export RUN_CMD="${tt}"
fi

# get json number of different compilers
nbConf=$(jq '.benchesComp | length' $JSON_INPUT)


#-------------------------------------------- local buildMe shell for each bench
BuildMeFile=buildme.sh
BuildCommandsOutfile=compileout.log
BuildCommandsErrfile=compileerr.log

cat << End
Building benchmarks.
Commands will be recorded in $BuildCommandsOutfile files, errors in $BuildCommandsErrfile.
You can use \`find -name $BuildCommandsOutfile\` and \`find -name $BuildCommandsErrfile\` to find them.
End


#---------------------- build for each bench, for each compiler, for each option
#
nbBench=0
nbBuild=0
nbFailedBuild=0
is_first_bench=y

for bench in $BENCH_LIST; do  # for each bench
  bench=$(echo $bench | xargs) # removed leading trailing space characters

  util.echoC "$SP bench=$bench, 'make $MAKETARGET'" $yellow

  ((nbBench++))

  if [ ! -d $bench ]; then
    util.echoC "$SP$SP$SP $bench not found!" $red
  else
    for ((i = 0; i < ${nbConf}; i++)); do # for each compiler
      # get from json the ieme compiler configuration
      bc_i=$(jq .benchesComp[$i] $JSON_INPUT)

      # get from json the compiler: CC
      tt=$(jq -n --argjson data "$bc_i" '$data.cc' | tr -d '"')
      export CC="${tt}"

      # get from json the flag: CCFLAG
      CCFLAG=$(jq -n --argjson data "$bc_i" '$data.cFlag' | tr -d '"')

      # get the prefix: CCPREFIX
      CCPREFIX=$(jq -n --argjson data "$bc_i" '$data.prefix' | tr -d '"')

      # get json the number of different options for this ieme compiler
      nbOpt=$(jq -n --argjson data "$bc_i" '$data.options | length')

      # ------------------------------------------------- trace infoOutFile
      #
      if [ $is_first_bench == 'y' ]; then
        echo "${preInf}CC=$CC" >> $infoOutFile
        echo "$($CC --version)" >>  $infoOutFile
      fi

      # for each option of a specific compiler
      for ((j = 0; j < ${nbOpt}; j++)); do
        ((nbBuild++))
        OPTION=$(jq .benchesComp[$i].options[$j] $JSON_INPUT)

        # get the flag OPT
        OPTCFLAG=$(jq -n --argjson data "$OPTION" '$data.cFlag' | tr -d '"')

        # get a prefix OPT
        OPTPREFIX=$(jq -n --argjson data "$OPTION" '$data.prefix' | tr -d '"')

        util.echoC "$SP$SP $CC (-- $CCPREFIX.$OPTPREFIX --)" $cyan
        # export PREFIX
        export PREFIX="$CCPREFIX.$OPTPREFIX"

        # export CFLAGS
        export CFLAGS="$CFLAGGLOB $CCFLAG $OPTCFLAG"

        export PROJ_SRC_DIR=$bench
        (cd $PROJ_SRC_DIR &&
           echo "VERIBENCH_ROOT='$VERIBENCH_ROOT' ARCH_AS='$ARCH_AS' ARCH_CC='$ARCH_CC' RUN_CMD='$RUN_CMD' CC='$CC' PREFIX='$PREFIX' CFLAGS='$CFLAGS' make $MAKEJOBS $MAKETARGET" >> $BuildMeFile &&
           make $MAKEJOBS $MAKETARGET \
                >>$BuildCommandsOutfile 2>>$BuildCommandsErrfile) ||
          { errlogs+="$bench/$BuildCommandsErrfile "; \
            util.echoC >&2 "$SP$SP$SP[ERROR][BUILD] Build failed ($bench $CCPREFIX.$OPTPREFIX)" $red; \
            ((nbFailedBuild++));  echo "[ERROR][BUILD] $bench, $CCPREFIX.$OPTPREFIX" >> $preGLOBAL_ERROR_LOG_FILE;  \
            }
      done # for each option of a specific compiler
    done  # for each compiler
    is_first_bench='n'
  fi
done  # for each bench


#-------------------------------------------------------- trace infoOutFile
#
echo "${preInf}jsonconf=$JSON_INPUT" >> $infoOutFile
echo "$(cat $JSON_INPUT)" >>  $infoOutFile
echo "${preInf}BENCH_LIST" >> $infoOutFile
echo "$BENCH_LIST" >>  $infoOutFile


# ----------------------------------------------------- trace faults and success
#
pre.traceInvalidPath
if [[ $nbFailedBuild -eq 0 ]]; then
  comment=" $(util.getRdGreatMsg)"
fi
util.echoC "#Bench=$nbBench, #Build=$nbBuild, #Failed=$nbFailedBuild.$comment" $yellow
if [[ $(($nbFailedBuild + $preNbInvalidPath)) -gt 0 ]]; then
  echo "------------------------------"
  util.echoC "bad path and/or build failed (see $preGLOBAL_ERROR_LOG_FILE):" $red
  cat $preGLOBAL_ERROR_LOG_FILE
  echo "------------------------------"
fi


# ----------------------------------------- eof buildBin.sh
#_____
