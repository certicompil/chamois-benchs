#!/bin/bash

green="\e[32m"
default="\e[39m"

scrptnm=$(basename $0)

if [ "$#" -ne 2 ]; then
    echo "Usage: $scrptnm <config.json> <list_benches.txt>" >&2
    exit 1
fi

rm -rf res/
mkdir res/
for i in {1..20} # Edit me!
do
        echo -e "${green}Run $i..${default}"
        ./cleanBenches.sh csvclean $2 > /dev/null 2>&1
        ./runBenches.sh $1 $2
        mv measures_cycles.csv res/measures_cycles.run$i.csv
done

echo -e "${green}Done.${default}"
