#!/bin/bash

green="\e[32m"
default="\e[39m"

lb=lb0_verimag.txt # Edit me!
conf=conf.aarch64.json # Edit me!

rm -rf res/
mkdir res/
for i in {1..20} # Edit me!
do
        echo -e "${green}Run $i..${default}"
        ./cleanBenches.sh csvclean benches/$lb > /dev/null 2&>1
        ./runBenches.sh conf/$conf benches/$lb
        mv measures_cycles.csv res/measures.run$i.csv
done

echo -e "${green}Done.${default}"
