rsync -avP --delete . $1:~/benches\
  --exclude='.git/'\
  --exclude='**/obj/'\
  --exclude='**/asm/'\
  --exclude='**/*.c'\
  --exclude='**/*.h'\
  --exclude='**/*.md'\
  --exclude='**/*.py'\
  --exclude='tmp/'\
  --exclude='timings-scripts/'\
  --exclude='tools/'\
  #--exclude='benches/tacle-bench/'\
  #--exclude='benches/polybench/'\
  #--exclude='benches/llvmtest/'\
  #--exclude='benches/mibench/'\
  #--exclude='benches/verimag/'\
  
