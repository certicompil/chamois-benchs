# Benchmarking `CompCert` and comparing it with others compilers

## Compiling `CompCert`

The first step to benchmark `CompCert` is to compile it - the `INSTALL.md` instructions of the project root folder should guide you on installing it.

For the benchmarks to work, the compiler `ccomp` should be on your `$PATH`, with the runtime libraries installed correctly (with a successful `make install` on the project root directory).

## jq (command-line JSON processor)

The configuration (parameters and/or compiler, ...) are defined in JSON data format.

*jq* is a lightweight and flexible command-line JSON processor. It is written in portable C, and it has zero runtime dependencies.

- [jq installation](https://github.com/stedolan/jq/wiki/Installation)
- [jq manual](https://stedolan.github.io/jq/manual/)

## Basic usage

- selected benches are listed in a text file `<lb_name.txt>`
- configuration is described in `<conf.name.json>`
- build binaries: `./buildBin.sh <conf.name.json> <lb_name.txt>`
- run binaries: `./runBenches.sh <conf.name.json> <lb_name.txt>`
- copy the benchmarks and script to the target machine (if cross-compiling): `./copyBenches.sh <name>`
- gather results in the file `measures_cycles.csv`

First, build binaries for each bench selected:

ex1. `./buildBin.sh <conf.name.json> <lb_name.txt>`

ex2. `cat benches/<lb_name.txt> | ./buildBin.sh <conf.name.json>`

The `<conf.name.json>` file describes the different compilers and options flag (see below for a complete description of the json format).


Second, run the bench:

`./runBenches.sh <conf.name.json> <lb_name.txt>`

The output of this script is a file named `measures_cycles.csv` containing the amount of cycles it took for each benchmark and each pair (compiler, flags).


Example of such output:

```
benches, gcc.O2, ccomp.nothing
llvmtest/ASCI_Purple, 24059052, 35152452
llvmtest/AMGmk, 669456648, 855108648
llvmtest/CrystalMk, 653341860, 1190263428
mibench/sha, 7145532, 3906720
```

If a crash happened during a run, or if the build previously failed, `ERROR` is displayed instead of the cycle count.

### useful

- cleaning all (standard) benches: `./cleanBenches.sh benches/lb0_*`

- building all (example): `time ./buildBin.sh conf/conf.x86_64.json benches/lb0_*`

```
real  12m15.745s
user  20m1.463s
sys   2m40.417s
```

- running all (example): `time ./runBenches.sh conf/conf.x86_64.json benches/lb0_*`

```
real  9m3.205s
user  8m55.754s
sys   0m8.806s
```

- copying benches to target machine: `./copyBenches <target>` (for instance, `<target>` is `marte` for the ARMv8 raspberry pi)

- run multiple times:
  - open `multiRun.sh` and edit the line with an "Edit me!" comment:
    the upper bound of the for loop might be changed to set the number of runs (20 is a good choice for high-confidence results)
  - `./multiRun.sh <conf.name.json> <lb_name.txt>`

**Remark:** When running benches on a distant machine, it is **highly recommended** launching the process inside tmux/screen to avoid losing the job if a connection problem arise.

## Json configuration file

The configuration file defines global parameters and different options for each compiler.

### global parameters:

-  `"runCMD":"taskset -c 0",`: fix the execution on a given core
-  `"cFlag": "-Wall -DPOLYBENCH_TIME -DVERIMAG -D_POSIX_C_SOURCE=200112L",`: flags common to every compiler
-  `"archAS":"clang -c",`: selected assembler to compile `.s` files
-  `"archCC":"gcc",`: compiler used to process the framework dependencies (here, it is recommended to always use a version of GCC corresponding to the targeted platform); the dependencies are files located in `com/`, and are used to retrieve the cycle counter content.

### compiler parameters

- `"benchesComp": [`: an array for each compiler :
  + `"cc": "gcc",`: the name of the executable compiler
  + `"cFlag": "-std=gnu99 -Wextra -ffp-contract=off",`: the global flags for this compiler
  + ` "prefix": "gcc",`:  the prefix to compose the id
  + `"options": [`:  an array for each specific flags
	    - `"cFlag": "-O2",`: the specific options
       - `"prefix": "O2"`: the prefix to compose the id

The id is composed as `compiler.prefix` concatenated with the `options.prefix` (ex. `gcc.O2`).

## Adding timings to a benchmark

If you just add a benchmark without any timing function, the resulting `measures_cycles.csv` file will be empty for lack of timing information output.

To add a timing, you must use the functions whose prototypes are in `veribench.h`

    
    #ifdef VERIBENCH
    #include "veribench.h"
    #endif

    /* ... */
    #ifdef VERIBENCH
    clock_prepare();
    #endif
 
    /* ... */
    #ifdef VERIBENCH
    clock_start();
    #endif

    /* .. computations we're timing .. */
    
    #ifdef VERIBENCH
    clock_stop();
    #endif

    /* ... */
    #ifdef VERIBENCH
    print_total_clock(); // print to stdout
    printerr_total_clock(); // print to stderr
    #endif

If the benchmark doesn't use `stdout` in a binary way you can use `print_total_clock()`. However, some benchmarks like `jpeg-6b` print their binary content to `stdout`, which then messes up the `grep` command when attempting to use it to extract the cycles from `stdout`.

The solution is then to use `printerr_total_clock()` which will print the cycles to `stderr`, and use `EXECUTE_ARGS` resembling this:
`EXECUTE_ARGS=-dct int -outfile __BASE__.jpg testimg.ppm 2> __BASE__.out`

`__BASE__` is a macro that gets expanded to the base name - that is, the `TARGET` concatenated the prefix. For instance, in `jpeg-6b`, `__BASE__` could be `jpeg-6b.ccomp.O2`.

## Multiple runs & analysis of results

When running the benches on a real hardware platform, the cycle counting method (see `com/veribench/cycles.h`) is generally not perfect, so the result may contain noise. Those inaccuracies are especially visible when running short tests (i.e. tests that execute quickly on the target core, as is the case for `benches/verimag/binary_search` for instance). A solution to tackle this problem is to perform multiple runs of the benchmarks (see the above explanations).

Once runs are finished, results will be store in `benches/res` on the target machine. The first step is to fetch them on your local machine (if you are not compiling and running on two different machines, skip this step):
`scp -r <user>@<target>:~/benches/res .`

Then, use the `plot.py` script to parse results, as follows:
- First make sure you have the following dependencies:
  - `python3`
  - `matplotlib`
  - `numpy`
  - `pandas`
  - `argparse`
- Launch `python3 plot.py <conf.name.json> <reference_column>`, where the second parameter is the reference you want to compare with. It is defined as the compiler prefix dot the option prefix. For instance, if my reference is CompCert with zero options, if the prefix for CompCert is `ccomp`, and if the prefix for the zero flag option is `nothing`, I would use `ccomp.nothing` as reference.
- Options:
  - `--nruns <N>`: number of runs (default is 0). If `N > 0` then the script will read in a folder `res` all the N CSV files.
  - `--size`: (default is false) an option to be used with the `fileSize.sh` script to compare size instead of runtime.
  - `--split <string>`: if the argument is a CSV file, cut dataframe according to selection intervals from the CSV file (e.g. see the file `benches/intervals.csv`); otherwise, if the argument is the string `prefix`, then the script will automatically split benches according to their prefix, which depends on the benchmark suite used (e.g. all polybench are prefixed with polybench, etc).
  - `--rsd <T>`: set a threshold `T` to filter benchmarks when the relative standard derivation (RSD) is above `T`. Default is 100% (no filter). This option is useful when running with multiple runs only. The RSD is computed from the reference column. When activated, this option will create a file `eliminated.csv` containing the list of filtered benchmarks.
  - `--input-filter <file>`: Eliminate benchmarks listed in `file`. When option is used, the RSD threshold is ignored.
  - `--md`, `--txt`, `--plot`, and `--latex`: indicate the output format of the script. Latex is not yet supported.

For example, when you have 20 runs of a benchmark suite, you can do: `python3 plot.py myconfig.json myrefcompiler.myrefoption --nruns 20 --rsd 2 --txt` to analyze all runs and filter benches with RSD above 2% (and output in text format).

The python script creates a folder `plot_output` containing its results, in the requested format (option plot generates an image containing the histogram chart).
