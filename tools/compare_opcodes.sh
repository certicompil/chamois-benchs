#!/bin/sh
extract_opcodes() {
    grep '^	' $1 |grep -v '^	\.' |cut -f2|sort|uniq > $2
}
s_to_insn() {
    echo $1 |sed -e 's/s$/insn/'
}
ccomp_s=$1
ccomp_insn=`s_to_insn $ccomp_s`
gcc_O2_s=`echo $ccomp_s | sed -e 's/ccomp\.nothing/gcc.O2/'`
gcc_O2_insn=`s_to_insn $gcc_O2_s`
comm=`echo $ccomp_s | sed -e 's/ccomp\.nothing\.s/comm/'`
extract_opcodes $ccomp_s $ccomp_insn
extract_opcodes $gcc_O2_s $gcc_O2_insn
comm -2 -3 $ccomp_insn $gcc_O2_insn > $comm
