#!/usr/bin/env bash

scrptnm=$(basename $0)
if [ "$#" -lt 1 ]; then
  echo "Usage: $scrptnm <config.json> [FILE]..." >&2
  exit 1
elif  ! [ -e "$1" ]; then
  echo  "$scrptnm: cannot access '$1': No such file or directory" >&2
elif  [ -d "$1" ]; then
  echo  "$scrptnm: '$1': Is a directory" >&2
elif  ! [ -s "$1" ]; then
  echo "$scrptnm: '$1': Is a zero size file" >&2
  exit 1
fi

# JSON_INPUT is non zero size file
JSON_INPUT=$1
shift

source com/preliminary.sh

# the list of relative path to benches selected 
BENCH_LIST=$listPath2Bench

OUTFILEPREFIX="measures_size"

DOTCSV=".csv"

FIRSTCSVLINE="benches"

getListPrefixFromJson() {
  LIST_PREFIX=
  local jsonf=$1 
  nbConf=$(jq '.benchesComp | length' $jsonf)
  for (( i=0; i<${nbConf}; i++ )); do
  	bc_i=$(jq .benchesComp[$i] $jsonf)
	  CCPREFIX=$(jq  -n --argjson data "$bc_i" '$data.prefix' | tr -d '"')
	  nbOpt=$(jq -n --argjson data "$bc_i"  '$data.options | length')
	  for (( j=0; j<${nbOpt}; j++ ));	do
	    OPTION=$(jq .benchesComp[$i].options[$j] $jsonf)
	    OPTPREFIX=$(jq -n --argjson data "$OPTION" '$data.prefix' | tr -d '"')
 	    LIST_PREFIX="$LIST_PREFIX $CCPREFIX.$OPTPREFIX"
	  done
  done
}

getAssemblyFromAsmFolder() {
  ASM_LIST=
  if test -d $1/asm; then
    ASM_LIST=$(ls -rt $1/asm/*.s)
  fi
}

getPrefixSum() {
  local prefix=$1
  shift
  SUM=
  while [ "$#" -gt 0 ]; do
    [[ $2 =~ (\.$prefix\.s$) ]]
    file="${BASH_REMATCH[1]}"
    if [ ! -z "$file" ]; then
      SUM=$(expr $SUM + $1)
    fi
    shift 2
  done
}

getListPrefixFromJson $JSON_INPUT

# extend first csv line
for prefix in $LIST_PREFIX; do
  FIRSTCSVLINE="$FIRSTCSVLINE, $prefix"
done

echo $FIRSTCSVLINE > $OUTFILEPREFIX$DOTCSV

cd benches
for bench in $BENCH_LIST; do
  echoC "Computing size for $bench" $yellow
  getAssemblyFromAsmFolder $bench
  SIZE_LIST=
  for asm in $ASM_LIST; do
    SIZE_LIST="$SIZE_LIST $(wc -l $asm)"
  done
  line=$(basename $bench)
  for prefix in $LIST_PREFIX; do
    getPrefixSum $prefix $SIZE_LIST
    line="$line, $SUM"
  done
  echo $line >> ../$OUTFILEPREFIX$DOTCSV
done
