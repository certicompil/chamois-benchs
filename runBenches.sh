#!/usr/bin/env bash


#-----------------------------
# usage: runBenches.sh conf.json [FILE]...
# - conf.json describe different configurations: compiler and options
# - FILE is either one (or more) path to a bench directory OR a text file
#   with a list of relative path to bench directory

##
# DEPENDANCE:
# jq :  command-line JSON processor,
# it is written in portable C, and it has zero runtime dependencies
#  - installation see @ https://github.com/stedolan/jq/wiki/Installation
##

##
# Options to this script:
# - $NO_RERUN: if active, will not rerun the benchmark if it was already run
# - $NO_PREFIX: if active, remove the prefix (veribench, polybench, ..)
#   to the benchmark in the list
# - $PRETTYP: (pretty-printing) if active, CSV file printing will be using
#   the PRETTYP_FORMAT
#
##


#
#----------------------------------------------------------------------- options

# NO_PREFIX="on"

# PRETTYP="on"
# PRETTYP_FORMAT="%16d"


#------------------------------------------------------------------- definitions
#
readonly FIRSTCSVLINE="benches"

# format output echo
readonly SP="#"

# measurement in cycles and/or nano seconds
readonly CYCLE_KWD="cycles"
readonly CLKNS_KWD="clk_ns"

# measurement results saved in file measures
readonly OUTFILEPREFIX="measures"
readonly DOTCSV=".csv"

# backup file suffix
readonly OUTBackMark="~"

# OUTFILEwTIMESTAMP="on"
readonly OUTFILEwTIMESTAMP="off"

# infoFile
readonly infoOutFile="infoRun.txt"

# TODO is usefull/useless ?
readonly SILENT="on"


#------------------------------------------------------------------------- begin
#
scrptnm=$(basename $0)
# echo "scrptnm='$scrptnm', arg1='$1'"


#----------------------------------------------------------- Usage and parameter
#
# check at least 1 argument (json configuration file)
#
if [ "$#" -lt 1 ]; then
  echo "Usage: $scrptnm <config.json> [FILE]..." >&2
  exit 1
elif  ! [ -e "$1" ]; then
  echo  "$scrptnm: cannot access '$1': No such file or directory.  Aborting" >&2
elif  [ -d "$1" ]; then
  echo  "$scrptnm: '$1': Is a directory" >&2
elif  ! [ -s "$1" ]; then
  echo "$scrptnm: '$1': Is a zero size file.  Aborting." >&2
  exit 1
fi
# JSON_INPUT is non zero size file
JSON_INPUT=$1
shift

# util.trace "(in $scrptnm:$LINENO) $0 $JSON_INPUT"


#------------------------------------------------------------ common preliminary
#

# get VERIBENCH_ROOT, listAbsPath2Bench
source com/preliminary.sh

if [ -z "$listAbsPath2Bench" ]; then
  pre.errmsg "[RUN] nothing to run."
  exit 1
fi

# the list of absolute paths selected
readonly BENCH_LIST=$listAbsPath2Bench
# util.trace "(in $scrptnm:$LINENO) BENCH_LIST: $BENCH_LIST"



#-------------------------------------------------------- trace infoOutfile
#
# info file ( to get a trace of os version, json conf, ..."
readonly preInf="#----------------------------------------------- "
if [ -e $infoOutFile ]; then
  mv $infoOutFile $infoOutFile$OUTBackMark
fi
> $infoOutFile
echo "${preInf}date:$(date +%Y%m%d%H%M%S)" >> $infoOutFile
echo "$(date) "  >> $infoOutFile
echo "${preInf}uname -a" >> $infoOutFile
echo "$(uname -a)" >>  $infoOutFile
util.getGitInfo
if [ $? == 0 ]; then
  echo "${preInf}git info" >> $infoOutFile
  echo $(git config --get remote.origin.url 2>/dev/null) >> $infoOutFile
  echo "GIT_BRANCH=${GIT_BRANCH}, GIT_REV=${GIT_REV}${GIT_DIFF}, GIT_TAG=${GIT_TAG}" >> $infoOutFile
fi

#------------------------------------------------------ get paramaters from json
#

# get json runCMD (value is passed to rules.mk via shell environment)
tt=$(jq '.runCMD' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
  export RUN_CMD="${tt}"
fi

tt=$(jq '.isKVX' $VERIBENCH_ROOT/$JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" -a "${tt}" == "true" ]; then
  export IS_KVX=true
fi

# get json kvxExecFile (value is passed to rules.mk via shell environment)
tt=$(jq '.kvxExecFile' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
  export KVX_EXECFILE="${tt}"
fi

# get json kvxArgs (value is passed to rules.mk via shell environment)
tt=$(jq '.kvxArgs' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
    export KVX_ARGS="${tt}"
fi

# get json clockCFlag (value is passed to rules.mk via shell environment)
tt=$(jq '.clockCFlag' $JSON_INPUT | tr -d '"')
if [ "${tt}" != "null" ]; then
  export CLK_CFLAG="${tt}"
  #    util.trace "(in $scrptnm:$LINENO) from json: CLK_CFLAG='$CLK_CFLAG'"
else
  export CLK_CFLAG=$DFLT_CLOCK_CFLAG
  #    util.trace "(in $scrptnm:$LINENO) from dflt: CLK_CFLAG='$CLK_CFLAG'"
fi


if [ `echo $CLK_CFLAG | grep -c "\-D[ ]*CYCLE_DISABLE" ` -gt 0 ]
then
  CYCLE_DISABLE=true
  CLOCK_ENABLE=true
fi

if [ `echo $CLK_CFLAG | grep -c "\-D[ ]*CLOCK_ENABLE" ` -gt 0 ]
then
  readonly CLOCK_ENABLE=true
fi

# util.trace "(in $scrptnm:$LINENO) CYCLE_DISABLE=$CYCLE_DISABLE"
# util.trace "(in $scrptnm:$LINENO) CLOCK_ENABLE=$CLOCK_ENABLE"

# ------------------------------------------- OUTPUT
# SILENT="on"
# Set SILENT to something to silence all run outputs ifdef SILENT SILENT=> /dev/null endif
# if [ -z ${SILENT+x} ]; then
#     echo 'SILENT is unset';
# else
#     SILENT='> /dev/null'; echo "SILENT is set to '$SILENT'";
# fi



#-------------------------------------------------------------------------------
#--------------------------------------------------------------------- functions
#


LIST_PREFIX=
# ----------------------------
# parse json to get LIST_PREFIX
# $1 : the json file to parse
# out parameter: LIST_PREFIX
function getListPrefixFromJson() {
  LIST_PREFIX=
  local jsonf=$1
  nbConf=$(jq '.benchesComp | length' $jsonf)
  local i
  for (( i=0; i<${nbConf}; i++ )); do
    bc_i=$(jq .benchesComp[$i] $jsonf)
    CCPREFIX=$(jq  -n --argjson data "$bc_i" '$data.prefix' | tr -d '"')
    nbOpt=$(jq -n --argjson data "$bc_i"  '$data.options | length')
    local j
    for (( j=0; j<${nbOpt}; j++ )); do
      OPTION=$(jq .benchesComp[$i].options[$j] $jsonf)
      OPTPREFIX=$(jq -n --argjson data "$OPTION" '$data.prefix' | tr -d '"')
      LIST_PREFIX="$LIST_PREFIX $CCPREFIX.$OPTPREFIX"
    done
  done
  # util.trace "(in $scrptnm:$LINENO) ${FUNCNAME[0]}: LIST_PREFIX= $LIST_PREFIX"
}



# ----------------------------
# get MEASURES in a Makefile
# if it is defined, the last line starting with "MEASURES=" e.g.:
# 'MEASURES="randomfill search1 ... search4" in verimag/binary_search'
# $1 : the file to parse
#
# affected variables: MEASURES, MEASURE_LIST
# if no line starting with MEASURES= in the Makefile then
#    MEASURES='', MEASURE_LIST='time'
# else
#    MEASURES and MEASURE_LIST are the same
function getMeasuresFromMakefile() {
  MEASURE_LIST=""
  MEASURES=""
  if test -f $1; then
    # ^ Matches the beginning of a line or string.
    eval $(grep "^MEASURES=" $1 | tail -1)
    MEASURE_LIST="${MEASURES:-time}"
  fi
  # util.trace "(in $scrptnm:$LINENO) ${FUNCNAME[0]}: MEASURE_LIST='$MEASURE_LIST', MEASURES='$MEASURES'"
}


# ----------------------------
# get binaries list by reverse modification time, oldest first
# from the bin folder of bench
# $1 : the path of the bench folder
# ex.: getBinariesFromBinFolder $BENCHES_ROOT/$bench
# affected variables: BIN_LIST
function getBinariesFromBinFolder() {
  BIN_LIST=
  if test -d $1/bin; then
    cd $1/bin
    # -r -t sort by reverse modification time, oldest first
    BIN_LIST=$(ls -r -t *.bin)
    cd ..
  fi
  # util.trace "(in $scrptnm:$LINENO) ${FUNCNAME[0]}: BIN_LIST='$BIN_LIST'"
}


# ----------------------------
# get binaries list from prefix list
# $1 : the specific bench
# $2 : The Prefix List
# ex.: getBinariesFromPrefixList $LIST_PREFI $bench
# affected variables: BIN_LIST
function getBinariesFromPrefixList() {
  BIN_LIST=
  local thebench="$1"
  # Shift all arguments to the left (original $1 gets lost)
  shift
  local arr=("$@")
  local i
  for i in "${arr[@]}"; do
    BIN_LIST+=" $(basename $thebench).$i.bin"
  done
  # util.trace "(in $scrptnm:$LINENO) ${FUNCNAME[0]}: BIN_LIST='$BIN_LIST'"
}



#######################################
#  get list of csv files
# Globals:
#
## Arguments:
# $1 : the bench
# $2 : the keyword (kwd)
# $3 : firstline
# Outputs:
#
#
######################################
function collectOutFiles() {

  kwd=$2
  util.echoC "$SP$SP gathering $kwd.out files (bench:'$1')" $cyan

  csvFile=$OUTFILEPREFIX"_"$kwd$DOTCSV
  echo $3 > $csvFile

  # for each measure: collect & parse each .out in out/ folder
  local i
  for i in $MEASURE_LIST; do
    fout="out/"$(echo $LIST_OUT | awk '{print $1}')
    if test -f "$fout"; then
      first=$(grep "$i $kwd"  $fout)
    else
      first=""
    fi

    if [ -n "$first" ]; then
      line=$(printf "$1")
      if [ $i != "time" ]; then
        line+=" $i"
      fi;

      for jout in $LIST_OUT; do
        util.echoC "$SP$SP$SP gathering .out for $kwd measure $i in $jout" $cyan
        if test -f "out/$jout"; then
          tt=$(grep "$i $kwd" out/$jout)
          val=$(echo $tt |  cut -d':' -f2)
        else
          ((nbFailedRun++))
          errStr="[RUN] $abspathBench:cannot access out/$jout  No such file or directory."
          pre.errmsg "$errStr"
          val=" "
        fi

# REMARK : -f4 instead of -f2 if there is the Kalray prefix on the printf

        sttr=" $(printf "%d" $val)"
        if [ -n "$PRETTYP" ]; then
          sttr="$(printf $PRETTYP_FORMAT $val)"
        fi
        line+=",$sttr"
      done
      echo "$line" >> $csvFile
    fi
  done
  # util.trace "(in $scrptnm:$LINENO) ${FUNCNAME[0]}: xxxx"
}




#######################################
# Gather global path of the measure csv file of each bench
# check if file exist and has more than 1 line
# Globals:
#  BENCH_LIST (used, unmodified)
#  csvFileList (set)
## Arguments:
#   $1 : name of the local measure csv file (e.g. 'measures_cycles.csv')
# Outputs:
#  csvFileList : list of csv measure file for each bench
#
######################################
function getCsvFileList() {
  csvFileList=""
  csvfile=$1
  local bench
  for bench in $BENCH_LIST; do
    util.echoC "$SP$SP $bench/$1" $yellow
    mfile=$bench/$1
    if [ -f $mfile ]; then
      nb_lines=$(wc $mfile | grep -o -E '[0-9]+' | head -1 | sed -e 's/^0\+//')
      if [ $nb_lines -gt 1 ]; then
        csvFileList+=" $mfile"
      else
        ((nbFailedRun++))
        errStr="[RUN] in $bench, $1 is ill-formed."
        pre.errmsg "$errStr"
      fi
    else
      ((nbFailedRun++))
      errStr="[RUN] cannot access $mfile: No such file or director."
      pre.errmsg "$errStr"
    fi
  done
}




#######################################
# outCsvFile prefix
# Globals:
#  OUTFILEwTIMESTAMP
#  csvFileList
## Arguments:
#  $1 : name prefix of the output file
# Outputs:
#  agregate from csvFileList all measure in output file
#  named the output file : $OUTFILEPREFIX[$DATESTAMP]$DOTCSV
######################################
function outCsvFile() {

  out=$1
  if [ "$OUTFILEwTIMESTAMP" = on ]; then
    out+="_$(date +%Y%m%d%H%M%S)"
  fi
  out+=$DOTCSV

  # rm if exist
  if [ -e $out ]; then
    mv $out $out$OUTBackMark
  fi

  if test -z "$csvFileList"
  then
    pre.errmsg "[RUN] empty list of csv files from benches, NO $out written."
  else
    if [ -z "$NO_PREFIX" ]; then
      is_first_bench=1
      #  rm -f $out

      # util.trace "(in $scrptnm:$LINENO) VERIBENCH_BENCHESF='$VERIBENCH_BENCHESF'"
      # util.trace "(in $scrptnm:$LINENO) csvFileList='$csvFileList'"

      local bench_csv
      for bench_csv in $csvFileList; do
        local benchroot=$(dirname $(dirname "$bench_csv"))
        if util.testPrefix $VERIBENCH_BENCHESF $benchroot; then
          benchroot=${benchroot#$VERIBENCH_BENCHESF/}
          # util.trace "(in $scrptnm:$LINENO) testPrefix OK"
        # else
          # util.trace "(in $scrptnm:$LINENO) testPrefix BAD"
        fi
        awk -v br="$benchroot/" -v ifb="$is_first_bench" '{
              if (NR != 1) { printf br; print }
              else {
                if (ifb == 0) { next }
                else { print }
              }
            }' $bench_csv >> $out
        # util.trace "(in $scrptnm:$LINENO) benchroot=$benchroot"
        is_first_bench=0
      done # for bench_csv in $csvFileList;

    else #  NO_PREFIX
      awk 'FNR==1 && NR!=1{next;}{print}' $csvFileList > $out
      # FNR is the number of lines (records) read so far in the current file
      # NR is the number of lines read overall.
      # So the condition 'FNR==1 && NR!=1{next;}' says,
      # "Skip this line if it's the first line of the current file,
      # and at least 1 line has been read overall."
      # This has the effect of printing the CSV header of the first
      # file while skipping it in the rest.
    fi
    util.echoC "$SP $out written" $cyan
  fi
}

# memo is first bench -> recopier la ligne d'entete en sortie
# memo awk, -v var=val --assign var=val



#-------------------------------------------------------------------------- main
#
#


RUNCOMMANDS_OUTFILE=runcommands.log
RUNCOMMANDS_ERRFILE=runcommandserr.log

cat << End
Running benchmarks.
Run commands will be recorded in $RUNCOMMANDS_OUTFILE files, errors in $RUNCOMMANDS_ERRFILE.
You can use \`find -name $RUNCOMMANDS_OUTFILE\` and \`find -name $RUNCOMMANDS_ERRFILE\` to find them.
End

getListPrefixFromJson $JSON_INPUT


#--------------- for each bench, for each config (compil/option), run the binary
#
nbBench=0
nbRun=0
nbFailedRun=0

for bench in $BENCH_LIST; do
  #for bench in $BENCH_LIST_REL; do

  ((nbBench++))
  util.echoC "$SP $bench" $cyan

  # ----------------------------------------------
  ## run .bin
  # ----------------------------------------------

  util.echoC "$SP$SP running binaries" $cyan
  export PROJ_SRC_DIR=$bench
  cd $PROJ_SRC_DIR

  mkdir -p out
  if [ -z "$NO_RERUN" ]; then
    rm -f  out/*.out
    rm -f  $RUNCOMMANDS_OUTFILE
    rm -f  $RUNCOMMANDS_ERRFILE
  fi
  abspathBench=$bench
  bench=$(basename $bench)
  # ------------- BIN_LIST
  getBinariesFromPrefixList $(basename $bench) $LIST_PREFIX
  # util.trace "(in $scrptnm:$LINENO) BIN_LIST fromJsonPrefixList='$BIN_LIST'"
  # util.trace "(in $scrptnm:$LINENO) BIN_LIST fromFolder='$BIN_LIST'"

  # ---------------------------------------------------------------
  # 1. for this bench, run the binaries (redirect stdout/stder to
  # the corresponding out/XXX.out file),
  # generate the first line of csv file.
  # ---------------------------------------------------------------
  firstline=$FIRSTCSVLINE
  LIST_OUT=
  for bb in $BIN_LIST;
  do
    ((nbRun++))
    CIBLE=${bb%.bin}.out
    util.echoC "$SP$SP$SP bin: $bb, cible: $CIBLE" $yellow
    if test -f "bin/$bb"; then
      (cd $abspathBench \
         && make out/$CIBLE  \
         SILENT="$SILENT" >> $RUNCOMMANDS_OUTFILE 2>> $RUNCOMMANDS_ERRFILE
      )
    else
      ((nbFailedRun++))
      errStr="[RUN] $abspathBench: cannot access bin/$bb No such file or directory."
      pre.errmsg "$errStr"
    fi
    tt=${CIBLE%.out}; ext=${tt#$(basename $(pwd)).}; base=${tt%.$ext}
    firstline+=", $ext"
    LIST_OUT="$LIST_OUT $CIBLE"
  done # bb in $BIN_LIST

  # util.trace "(in $scrptnm:$LINENO) FIRSTCSVLINE='$firstline'"
  # util.trace "(in $scrptnm:$LINENO) LIST_OUT='$LIST_OUT'"


  # ---------------------------------------------------------------
  ## 2.  collect & parse each .out files for this bench
  # ---------------------------------------------------------------

  # ------------- MEASURE_LIST
  getMeasuresFromMakefile $abspathBench/Makefile
  # util.trace "(in $scrptnm:$LINENO) MEASURE_LIST='$MEASURE_LIST'"

  # set the first line for csv file
  if [ ! "$CYCLE_DISABLE" = true ]; then
    collectOutFiles $bench $CYCLE_KWD "$firstline"
  fi

  # set the first line for csv file
  if [ "$CLOCK_ENABLE" = true ]; then
    collectOutFiles $bench $CLKNS_KWD "$firstline"
  fi

  printf "\n"
done # bench in $BENCH_LIST

# ----------------------------------------------
## Gather all the CSV files
# ----------------------------------------------


# --------------------- gathering all csv files in a unique measure csv file

cd $VERIBENCH_ROOT

csvFileList=""
# "cycle" csv files
if [[ ! "$CYCLE_DISABLE" = true ]]; then
  csvFile=$OUTFILEPREFIX"_"$CYCLE_KWD$DOTCSV
  util.echoC "$SP gathering all $csvFile files" $cyan
  getCsvFileList $csvFile
  outCsvFile $VERIBENCH_ROOT/$OUTFILEPREFIX"_"$CYCLE_KWD
fi

# clock nanoseconds csv file
if [[ "$CLOCK_ENABLE" = true ]]; then
  csvFile=$OUTFILEPREFIX"_"$CLKNS_KWD$DOTCSV
  util.echoC "$SP gathering all $csvFile files" $cyan
  getCsvFileList $csvFile
  outCsvFile $VERIBENCH_ROOT/$OUTFILEPREFIX"_"$CLKNS_KWD
fi


#-------------------------------------------------------- trace infoOutFile
#
echo "${preInf}jsonconf=$JSON_INPUT" >> $infoOutFile
echo "$(cat $JSON_INPUT)" >>  $infoOutFile
echo "${preInf}BENCH_LIST" >> $infoOutFile
echo "$BENCH_LIST" >>  $infoOutFile

# ----------------------------------------- trace faults and success

pre.traceInvalidPath

if [[ $nbFailedRun -eq 0 ]]; then
  comment=" $(util.getRdGreatMsg)"
fi
util.echoC "#Bench=$nbBench, #Run=$nbRun, #Failed=$nbFailedRun.$comment" $yellow
if [[ $(($nbFailedRun + $preNbInvalidPath)) -gt 0 ]]; then
  echo "------------------------------"
  util.echoC "bad path and/or run failed (see $preGLOBAL_ERROR_LOG_FILE):" $red
  cat $preGLOBAL_ERROR_LOG_FILE
  echo "------------------------------"
fi

exit 0

# ------------------------------------------- eof runBenches.sh
#_____
