# benches/README.md

## Tests

This directory contains the following tests suites:
- llvmtest `benches/llvmtest` (various applicative tests)
[git repo](https://github.com/lac-dcc/Benchmarks)
- mibench `benches/mibench` (embedded)
The original MiBench code as downloaded from [here](https://vhosts.eecs.umich.edu/mibench/) is held in the master branch.
- polybench `benches/polybench` (polyhedral calculus)
[official site](http://web.cse.ohio-state.edu/~pouchet.2/software/polybench/)
- tacle-bench `benches/tacle-bench` (embedded)
[git repo](https://github.com/tacle/tacle-bench)
- verimag `benches/verimag` (our own test suite, from simple sort algorithms to complete applications like ocaml)

## Lists of benchmarks

Files whose name is starting with `lb0_` contains the whole list of supported benchmarks for each suite.
The supported list can be a subset of the original suite: for instance, for llvm tests, many tests of the original repository are not yet supported (see `lb0_llvmtest.txt`).

The other list files are named `lb_`, and are custom sets of benchmarks. They either define smaller sets composed of several tests from various suites, or are subsets of `lb0_`'s suites for specific architectures (because some tests are not relevant or compatible on some architectures).