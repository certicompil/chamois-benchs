/* NIST Secure Hash Algorithm */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "sha.h"

#ifdef VERIBENCH
#include "veribench.h"
#endif

int main(int argc, char **argv)
{
    FILE *fin;
    SHA_INFO sha_info;

    if (argc < 2) {
	fin = stdin;
	sha_stream(&sha_info, fin);
	sha_print(&sha_info);
    } else {
	while (--argc) {
	    fin = fopen(*(++argv), "rb");
	    if (fin == NULL) {
		printf("error opening %s for reading\n", *argv);
	    } else {

#ifdef VERIBENCH
  clock_prepare();  clock_start();
#endif	
		sha_stream(&sha_info, fin);
		sha_print(&sha_info);

#ifdef VERIBENCH
  clock_stop();  print_total_clock();
#endif		
		fclose(fin);
	    }
	}
    }
    return(0);
}
