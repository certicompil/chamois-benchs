/* testc - Test adpcm coder */

#include "adpcm.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifdef VERIBENCH
#include "veribench.h"
#endif

struct adpcm_state state;

#define NSAMPLES 1000

char	abuf[NSAMPLES/2];
short	sbuf[NSAMPLES];

int main() {
  int n;
  
#ifdef VERIBENCH
  clock_prepare();  clock_start();
#endif

  while(1) {
    n = read(0, sbuf, NSAMPLES*2);
    if ( n < 0 ) {
      perror("input file");
      exit(1);
    }
    if ( n == 0 ) break;
    adpcm_coder(sbuf, abuf, n/2, &state);
    //write(1, abuf, n/4);
  }

#ifdef VERIBENCH
  clock_stop();  print_total_clock();
#endif
  
  fprintf(stderr, "Final valprev=%d, index=%d\n",
      state.valprev, state.index);
  exit(0);
}
