
/****
    Copyright (C) 1996 McGill University.
    Copyright (C) 1996 McCAT System Group.
    Copyright (C) 1996 ACAPS Benchmark Administrator
                       benadmin@acaps.cs.mcgill.ca

    This program is free software; you can redistribute it and/or modify
    it provided this copyright notice is maintained.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
****/

/**************************************************************************************/
/*                                                                                    */
/*  Title       : Assignment #1 - Benchmark for McCat compiler                        */
/*                                                                                    */
/*  Course      : CS621 - Advanced Compiler Techniques (Clark Verbrugge)              */
/*                                                                                    */
/*  Filename    : bnchmrk.c                                                           */
/*                                                                                    */
/*  Author      : Frederic Bergeron (91 485 12)                                       */
/*                                                                                    */
/*  Date        : 1996/10/01                                                          */
/*                                                                                    */
/*  Description : This program is a benchmark for the McCat Compiler.  Its main       */
/*                goal is to test recursive calls and dynamic allocation.  It         */
/*                uses a simple binary tree structure of integers.  It computes       */
/*                the arithmetic average of all the numbers in the tree and           */
/*                performs some research in the tree.                                 */
/*                                                                                    */
/**************************************************************************************/


#include "general.h"
#include "bintree.h"


#ifdef VERIBENCH
#include "veribench.h"
#endif

#define MAX_SEARCHED_VALUES 	100
#define TRACE 			0


/**************************************************************************************/
/* Function declarations                                                              */
/**************************************************************************************/

void	fillTree(		struct binaryTree**	treeToFill	);
void    fillSearchValues( 	int 			arrayOfValues[] );

/**************************************************************************************/


/**************************************************************************************/
void	fillTree(	struct binaryTree**	treeToFill	)
/**************************************************************************************/
{
	int	number;

	printf( "Constructing tree\n\n\n" );
	
	scanf( "%d", &number );

	while (number != 0) {
	
		insertSortedBinaryTree( number, treeToFill ); 
		if (TRACE)
		  printf ("%d inserted\n", number);
		scanf( "%d", &number );
	}
	
#ifndef VERIBENCH
	printf( "\n\nTree constructed\n\n\n" );
#endif
}
/**************************************************************************************/


/**************************************************************************************/
void 	fillSearchedValues( 	int 	arrayOfValues[] 	)
/**************************************************************************************/
{
	int	number, i = 0;

#ifndef VERIBENCH
	printf( "Constructing array of values\n\n\n" );
#endif	
	scanf( "%d", &number ); 

	while ( (i<MAX_SEARCHED_VALUES) && (number!=0) ) {
		
		arrayOfValues[i] = number;
		if (TRACE)
		  printf ("%d read; %d inserted\n", number, arrayOfValues[i] );
		scanf( "%d", &number );
		i++;
	}
	
#ifndef VERIBENCH
	printf( "\n\nArray of values constructed\n\n\n" );
#endif
}
/**************************************************************************************/


/**************************************************************************************/
int main()
/**************************************************************************************/
{
	struct binaryTree*	tree = NULL;
        int                     searchedValues[MAX_SEARCHED_VALUES];
	int                     i;

	int sizeBT   = 0;
	double sumBT  = 0.0;
	double meanBT = 0.0;
	double meanBTopt = 0.0;

#ifndef VERIBENCH
	printf( "Beginning of program\n\n\n" );
#endif


#ifdef VERIBENCH
   clock_prepare(); clock_start();
#endif

	for( i=0; i<10; i++) 
		searchedValues[i]=0;
	
	fillTree( &tree );
        fillSearchedValues( searchedValues );

	sizeBT = getSizeBinaryTree( tree );
	sumBT  = getSumBinaryTree( tree );
	meanBT = getArithmeticMeanBinaryTree( tree );
	meanBTopt = getArithmeticMeanOptimized( tree );
	
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("fill");
#endif

	
	if (TRACE) {
	  printBinaryTree( tree );
	  printf( "\n\n" );
	  printSortedBinaryTree( tree );
	  printf( "\n\n" );
	}

	printf( "Summary of sorted binary tree\n=============================\n" );
	printf( "Size                   : %d\n", sizeBT );
	printf( "Sum                    : %f\n", sumBT );
	printf( "Arithmetic Mean        : %f\n", meanBT );
	printf( "Arithmetic Mean (opt.) : %f\n", meanBTopt );
	printf( "stupid sum             : %f\n\n", sizeBT + sumBT + meanBT + meanBTopt);


#ifdef VERIBENCH
	long cumul =  0;
	clock_reset(); clock_start();
#endif	
	for( i=0; i<MAX_SEARCHED_VALUES; i++) {
	  if (memberOfBinaryTree( tree, searchedValues[i] )) {
	    cumul += searchedValues[i];
#ifndef VERIBENCH
	    printf( "%d is in the tree.\n",	searchedValues[i] );
#endif
	  }
	  else {
	    cumul -= searchedValues[i];
#ifndef VERIBENCH
	    printf( "%d is NOT in the tree.\n", searchedValues[i] );
#endif
	  }
	}
	  
#ifdef VERIBENCH
	  clock_stop();  printstr_total_clock("search"); printf("cumul = %d\n", cumul);
#endif


#ifndef VERIBENCH
	printf( "\n\n" );
//	for( i=0; i<MAX_SEARCHED_VALUES; i++) {
//		if (memberOfSortedBinaryTree( tree, searchedValues[i] ))
//			printf( "%d is in the tree.\n",	searchedValues[i] );
//		else
//			printf( "%d is NOT in the tree.\n", searchedValues[i] );
//	}
//	printf( "\n\n" );
	
	printf( "\n\nEnd of program\n" );
#endif
        return 0;
}
/**************************************************************************************/




