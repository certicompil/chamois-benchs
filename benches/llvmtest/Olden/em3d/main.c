/* For copyright information, see olden_v1.0/COPYRIGHT */

#include "em3d.h"
#include "make_graph.h"

#ifdef VERIBENCH
#include "veribench.h"
#define chatting do_nothing
#endif

extern int NumNodes;

int DebugFlag;

void print_graph(graph_t *graph, int id) 
{
  node_t *cur_node;
  cur_node=graph->e_nodes[id];

  for(; cur_node; cur_node=cur_node->next)
    {
      chatting("E: value %f, from_count %d\n", *cur_node->value,
	       cur_node->from_count);
    }
  cur_node=graph->h_nodes[id];
  for(; cur_node; cur_node=cur_node->next)
    {
      chatting("H: value %f, from_count %d\n", *cur_node->value,
	       cur_node->from_count);
    }
}

extern int nonlocals;

int main(int argc, char *argv[])
{
  int i;
  graph_t *graph;

  DebugFlag = 0;
  NumNodes = 1;
#ifndef VERIBENCH
  dealwithargs(argc,argv);
#elif SMALL_PROBLEM_SIZE
  n_nodes = 256;
  d_nodes = 250;
  local_p = 35;
#else
  n_nodes = 1024;
  d_nodes = 1000;
  local_p = 125;
#endif

#ifdef VERIBENCH
  printf ("Hello world--Doing em3d with args %d %d %d %d\n",
           n_nodes,d_nodes,local_p,NumNodes);
  clock_prepare(); clock_start();
#else
  chatting("Hello world--Doing em3d with args %d %d %d %d\n",
           n_nodes,d_nodes,local_p,NumNodes);
#endif

  graph=initialize_graph();
  if (DebugFlag) 
    for(i=0; i<NumNodes;i++)
      print_graph(graph,i);


  compute_nodes(graph->e_nodes[0]);
  compute_nodes(graph->h_nodes[0]);
  
#ifdef VERIBENCH
    clock_stop(); print_total_clock();
#endif

    chatting("nonlocals = %d\n",nonlocals);

  printstats();
  return 0;
}
