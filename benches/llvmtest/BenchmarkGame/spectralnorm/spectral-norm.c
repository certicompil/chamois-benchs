/* -*- mode: c -*-
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Sebastien Loisel
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifdef VERIBENCH
#include "veribench.h"
#endif

double eval_A(int i, int j) { return 1.0/((i+j)*(i+j+1)/2+i+1); }

void eval_A_times_u(int N, const double u[], double Au[])
{
  int i,j;
  for(i=0;i<N;i++)
    {
      Au[i]=0;
      for(j=0;j<N;j++) Au[i]+=eval_A(i,j)*u[j];
    }
}

void eval_At_times_u(int N, const double u[], double Au[])
{
  int i,j;
  for(i=0;i<N;i++)
    {
      Au[i]=0;
      for(j=0;j<N;j++) Au[i]+=eval_A(j,i)*u[j];
    }
}

//void eval_AtA_times_u(int N, const double u[], double AtAu[])
//{ double v[N]; eval_A_times_u(N,u,v); eval_At_times_u(N,v,AtAu); }

// #define N 2000
#define N 200

int main(int argc, char **argv)
{
  int i;
  //  int N = ((argc == 2) ? atoi(argv[1]) : 2000);
  //  int N = 2000;
  
  double u[N],v[N],vBv,vv;

  double w[N];
  
#ifdef VERIBENCH
  clock_prepare(); clock_start();
#endif
    
  for(i=0;i<N;i++) u[i]=1;
  for(i=0;i<10;i++)
    {
      // eval_AtA_times_u(N,u,v);
      eval_A_times_u(N,u,w);
      eval_At_times_u(N,w,v);
      
      // eval_AtA_times_u(N,v,u);
      eval_A_times_u(N,v,w);
      eval_At_times_u(N,w,u);
     
    }
  vBv=vv=0;
  for(i=0;i<N;i++) { vBv+=u[i]*v[i]; vv+=v[i]*v[i]; }

#ifdef VERIBENCH
  clock_stop(); print_total_clock();
#endif
  
  printf("%0.9f\n",sqrt(vBv/vv));
  return 0;
}
