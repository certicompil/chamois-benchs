/*BHEADER****************************************************************
 * (c) 2006   The Regents of the University of California               *
 *                                                                      *
 * See the file COPYRIGHT_and_DISCLAIMER for a complete copyright       *
 * notice and disclaimer.                                               *
 *                                                                      *
 *EHEADER****************************************************************/


//--------------
//  A micro kernel based on IRS
//    http://www.llnl.gov/asci/purple/benchmarks/limited/irs/
//--------------


#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include "irsmk.h"

#ifdef VERIBENCH
#include "veribench.h"
#endif

void allocMem(RadiationData_t *, int);
void init(Domain_t *, RadiationData_t *, double *, double *, Input_t *);
void readInput(const char *, Input_t *);
void rmatmult3(Domain_t *, RadiationData_t *, double *, double *);


int main(int argc, const char *argv[])
{
  Domain_t domain;
  Domain_t *domain_ptr = &domain;

  RadiationData_t rblk;
  RadiationData_t *rblk_ptr = &rblk;

  struct timeval  t0, t1;
  clock_t t0_cpu = 0,
          t1_cpu = 0;

  double *x;
  double *b;

  int i = 0;
#ifdef SMALL_PROBLEM_SIZE
  const int noIter = 250;
#else
  const int noIter = 5000;
#endif

  printf ("\nSequoia Benchmark Version 1.0 (noIter=%d)\n\n", noIter);

  if (argc != 2) {
    printf("Usage: %s <input>\n", argv[0]);
    return 1;
  }
  //

  Input_t inputArgs;

  readInput(argv[1], &inputArgs);

  b = (double *)malloc(inputArgs.i_ub*sizeof(double));
  x = (double *)malloc(inputArgs.x_size*sizeof(double));
  
  allocMem(rblk_ptr, inputArgs.i_ub);

#ifdef VERIBENCH
  clock_prepare(); clock_start();
#endif

  init(domain_ptr, rblk_ptr, x, b, &inputArgs);

  for (i=0; i<noIter; ++i) {
     rmatmult3(domain_ptr, rblk_ptr, x, b);
  }

#ifdef VERIBENCH
  clock_stop(); print_total_clock();
#endif

  printf("***** results \n");  
  for (i=0; i<inputArgs.i_ub; i+=inputArgs.i_ub/5) {
    printf("i = %10d      b[i] = %e \n", i, b[i]);
  }

  return(0);
}
