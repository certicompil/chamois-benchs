#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#ifdef VERIBENCH
#include "veribench.h"
#endif

#define NLOOP 10000


typedef int data;
typedef unsigned index;

/* from Rosetta code */
int my_bsearch (data *a, index n, data x) {
    index i = 0, j = n - 1;
    while (i <= j) {
        index k = (i + j) / 2;
        if (a[k] == x) {
            return k;
        }
        else if (a[k] < x) {
            i = k + 1;
        }
        else {
            j = k - 1;
        }
    }
    return -1;
}

int my_bsearch2 (data *a, index n, data x) {
    index i = 0, j = n - 1;
    while (i <= j) {
        index k = (i + j) / 2;
	index kp1 = k+1, km1 = k-1;
	data ak = a[k];
	i = ak < x ? kp1 : i;
        j = ak > x ? km1 : j;
        if (ak == x) {
            return k;
        }
    }
    return -1;
}

int my_bsearch3 (data *a, index n, data x) {
    index i = 0, j = n - 1;
    while (i <= j) {
        index k = (i + j) / 2;
	index kp1 = k+1, km1 = k-1;
	data ak = a[k];
	_Bool lt = ak < x, gt = ak > x;
	i = lt ? kp1 : i;
	j = gt ? km1 : j;
        if (ak == x) {
            return k;
        }
    }
    return -1;
}

int my_bsearch4 (data *a, index n, data x) {
  index i = 0, j = n - 1, k;
  k = (i + j) / 2;
  while (i <= j) {
    index kp1 = k+1, km1 = k-1;
    data ak = a[k];
    _Bool lt = ak < x, gt = ak > x;
    i = lt ? kp1 : i;
    j = gt ? km1 : j;
    if (ak == x) {
      goto end;
    }
    k = (i + j) / 2;
  }
  k=-1;
 end:
  return k;
}

void random_ascending_fill(data *a, index n) {
  unsigned r = 41;
  data v = 0;
  for(index i=0; i<n; i++) {
    a[i] = v;
    v++;
    v = (r & 0x40000000) ? (v+1) : v;
    r = r * 97 + 5;
  }
}

int main () {
  int i;
  index n=25000;
  index cumul=0;
  data v=1502;
  data *buf=malloc(n*sizeof(data));
  
#ifdef VERIBENCH
  clock_prepare(); clock_reset(); clock_start();
#endif

  //  for(i=0;i<NLOOP;i++){
    random_ascending_fill(buf, n);
    // }

#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("randomfill");
#endif


  index pos1 = my_bsearch(buf, n, v); // load code in cache, call the function one time before  measuring
#ifdef VERIBENCH
  clock_reset(); clock_start();
#endif
  for(i=0;i<NLOOP;i++){
    cumul += my_bsearch(buf, n, v);
  }
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("search1");
#endif
  
  index pos2 = my_bsearch2(buf, n, v);// load code in cache,...
#ifdef VERIBENCH
  clock_reset(); clock_start();
#endif
  for(i=0;i<NLOOP;i++){
    cumul += my_bsearch2(buf, n, v);
  }
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("search2");
#endif

  index pos3 = my_bsearch3(buf, n, v);// load code in cache,...
#ifdef VERIBENCH
  clock_reset(); clock_start();
#endif
  for(i=0;i<NLOOP;i++){
    cumul += my_bsearch3(buf, n, v);
  }
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("search3");
#endif
  

  index pos4 = my_bsearch4(buf, n, v);// load code in cache,...
#ifdef VERIBENCH
  clock_reset(); clock_start();
#endif
  for(i=0;i<NLOOP;i++){
    cumul += my_bsearch4(buf, n, v);
  }
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("search4");
#endif


  printf("position1: %d\n"
	 "position2: %d\n"
	 "position3: %d\n"
	 "position4: %d\n"
	 "cumul: %d\n"
	 "cumul/(4*NLOOP): %d\n"
	 ,pos1 ,pos2 ,pos3 ,pos4 ,cumul ,(cumul/(4*NLOOP))
	 );
  free(buf);
}
