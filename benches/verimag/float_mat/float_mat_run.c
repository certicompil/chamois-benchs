#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "float_mat.h"

#ifdef VERIBENCH
#include "veribench.h"
#endif

/* FIXME DMonniaux should be in the other but branches and float_of_int not implemented */
bool REAL_mat_equal(unsigned m,
		    unsigned n,
		    const REAL *a, unsigned stride_a,
		    const REAL *b, unsigned stride_b) {
  for(unsigned i=0; i<m; i++) {
    for(unsigned j=0; j<n; j++) {
      if (a[i*stride_a + j] != b[i*stride_b + j]) {
	printf("at %u,%u: %g vs %g\n", i, j,
	       a[i*stride_a + j], b[i*stride_b + j]);
	return false;
      }
    }
  }
  return true;
}

REAL REAL_random(void) {
  static uint64_t next = 1325997111;
  next = next * 1103515249 + 12345;
  return next % 1000;
}

void REAL_mat_random(unsigned m,
		     unsigned n,
		     REAL *a, unsigned stride_a) {
  for(unsigned i=0; i<m; i++) {
    for(unsigned j=0; j<n; j++) {
      a[i*stride_a + j] = REAL_random();
    }
  }
}

int main() {
  const unsigned m = 60, n = 31, p = 50;
  
  REAL *a = malloc(sizeof(REAL) * m * n);
  REAL *b = malloc(sizeof(REAL) * n * p);
  REAL_mat_random(m, n, a, n);
  REAL_mat_random(n, p, b, p);
  
  REAL *c1 = malloc(sizeof(REAL) * m * p);
  REAL *c2 = malloc(sizeof(REAL) * m * p);
  REAL *c3 = malloc(sizeof(REAL) * m * p);
  REAL *c4 = malloc(sizeof(REAL) * m * p);
  REAL *c5 = malloc(sizeof(REAL) * m * p);
  REAL *c6 = malloc(sizeof(REAL) * m * p);
  REAL *c7 = malloc(sizeof(REAL) * m * p);
  REAL *c8 = malloc(sizeof(REAL) * m * p);

#ifdef VERIBENCH
  clock_prepare(); clock_start();
#endif
  
  REAL_mat_mul1(m, n, p, c1, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c1"); clock_reset(); clock_start();
#endif
  
  REAL_mat_mul2(m, n, p, c2, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c2"); clock_reset();  clock_start();
#endif
 
  REAL_mat_mul3(m, n, p, c3, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c3"); clock_reset();  clock_start();
#endif
  
  REAL_mat_mul4(m, n, p, c4, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c4"); clock_reset();  clock_start();
#endif
  
  REAL_mat_mul5(m, n, p, c5, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c5"); clock_reset();  clock_start();
#endif
  
  REAL_mat_mul6(m, n, p, c6, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c6"); clock_reset();  clock_start();
#endif
  
  REAL_mat_mul7(m, n, p, c7, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c7"); clock_reset();  clock_start();
#endif
  
  REAL_mat_mul8(m, n, p, c8, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c8");
#endif
  
  printf("c1==c2: %s\n"
	 "c1==c3: %s\n"
	 "c1==c4: %s\n"
	 "c1==c5: %s\n"
	 "c1==c6: %s\n"
	 "c1==c7: %s\n"
	 "c1==c8: %s\n"
	
	 , REAL_mat_equal(m, n, c1, p, c2, p)?"true":"false"
	 , REAL_mat_equal(m, n, c1, p, c3, p)?"true":"false"
	 , REAL_mat_equal(m, n, c1, p, c4, p)?"true":"false"
	 , REAL_mat_equal(m, n, c1, p, c5, p)?"true":"false"
	 , REAL_mat_equal(m, n, c1, p, c6, p)?"true":"false"
	 , REAL_mat_equal(m, n, c1, p, c7, p)?"true":"false"
	 , REAL_mat_equal(m, n, c1, p, c8, p)?"true":"false"
	 );
  
  free(a);
  free(b);
  free(c1);
  free(c2);
  free(c3);
  free(c4);
  free(c5);
  free(c6);
  free(c7);
  free(c8);
  return 0;
}
