#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "xor_and.h"

#ifdef VERIBENCH
#include "veribench.h"
#endif

int main() {
  const unsigned m = 60, n = 31, p = 50;

  xor_and *a = malloc(sizeof(xor_and) * m * n);
  xor_and *b = malloc(sizeof(xor_and) * n * p);
  xor_and_mat_random(m, n, a, n);
  xor_and_mat_random(n, p, b, p);

  xor_and *c1 = malloc(sizeof(xor_and) * m * p);
  xor_and *c2 = malloc(sizeof(xor_and) * m * p);
  xor_and *c3 = malloc(sizeof(xor_and) * m * p);
  xor_and *c4 = malloc(sizeof(xor_and) * m * p);
  xor_and *c5 = malloc(sizeof(xor_and) * m * p);
  xor_and *c6 = malloc(sizeof(xor_and) * m * p);
  xor_and *c7 = malloc(sizeof(xor_and) * m * p);
    
#ifdef VERIBENCH
  clock_prepare(); clock_start();
#endif 
  xor_and_mat_mul1(m, n, p, c1, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c1"); clock_reset(); clock_start();
#endif

  xor_and_mat_mul2(m, n, p, c2, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c2"); clock_reset(); clock_start();
#endif
  
  xor_and_mat_mul3(m, n, p, c3, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c3"); clock_reset(); clock_start();
#endif

  xor_and_mat_mul4(m, n, p, c4, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c4"); clock_reset(); clock_start();
#endif

  xor_and_mat_mul5(m, n, p, c5, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c5"); clock_reset(); clock_start();
#endif

  xor_and_mat_mul6(m, n, p, c6, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c6");  clock_reset(); clock_start();
#endif

  xor_and_mat_mul7(m, n, p, c7, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c7");
#endif

  
  printf("c1==c2: %s\n"
	 "c1==c3: %s\n"
	 "c1==c4: %s\n"
	 "c1==c5: %s\n"
	 "c1==c6: %s\n"
	 "c1==c7: %s\n"

	 , xor_and_mat_equal(m, n, c1, p, c2, p)?"true":"false"
	 , xor_and_mat_equal(m, n, c1, p, c3, p)?"true":"false"
	 , xor_and_mat_equal(m, n, c1, p, c4, p)?"true":"false"
	 , xor_and_mat_equal(m, n, c1, p, c5, p)?"true":"false"
	 , xor_and_mat_equal(m, n, c1, p, c6, p)?"true":"false"
	 , xor_and_mat_equal(m, n, c1, p, c7, p)?"true":"false"
	 );
  
  free(a);
  free(b);
  free(c1);
  free(c2);
  free(c3);
  free(c4);
  free(c5);
  free(c6);
  free(c7);
  return 0;
}
