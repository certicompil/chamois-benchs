
#ifdef VERIBENCH
#include "veribench.h"
#endif

int picosat_main (int, char **);

int
main (int argc, char **argv)
{

#ifdef VERIBENCH
  clock_prepare();
  clock_start();
#endif

  int ret= picosat_main (argc, argv);

#ifdef VERIBENCH
  clock_stop();
  print_total_clock();
#endif

  return ret;
}
