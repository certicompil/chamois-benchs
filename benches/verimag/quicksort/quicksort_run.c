#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "quicksort.h"

#ifdef VERIBENCH
#include "veribench.h"
#endif

int main (void) {

  unsigned len=30000;
  data *vec = malloc(sizeof(data) * len);
  data_vec_random(vec, len);
  
#ifdef VERIBENCH
  clock_prepare(); clock_start();
#endif  

  quicksort(vec, len);
  
#ifdef VERIBENCH
  clock_stop(); print_total_clock();
#endif 

  printf("sorted=%s\n"
	 , data_vec_is_sorted(vec, len)?"true":"false"
	 );
  free(vec);
  return 0;
}
 
