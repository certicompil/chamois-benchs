#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "modint.h"

#ifdef VERIBENCH
#include "veribench.h"
#endif


int main() {

  const unsigned m = 60, n = 31, p = 50;

  modint *a = malloc(sizeof(modint) * m * n);
  modint *b = malloc(sizeof(modint) * n * p);
  modint_mat_random(m, n, a, n);
  modint_mat_random(n, p, b, p);

  modint *c1 = malloc(sizeof(modint) * m * p);
  modint *c2 = malloc(sizeof(modint) * m * p);
  modint *c3 = malloc(sizeof(modint) * m * p);
  modint *c4 = malloc(sizeof(modint) * m * p);
  modint *c5 = malloc(sizeof(modint) * m * p);
  modint *c6 = malloc(sizeof(modint) * m * p);
  modint *c7 = malloc(sizeof(modint) * m * p);

#ifdef VERIBENCH
  clock_prepare(); clock_start();
#endif

  modint_mat_mul1(m, n, p, c1, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c1"); clock_reset(); clock_start();
#endif

  modint_mat_mul2(m, n, p, c2, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c2"); clock_reset(); clock_start();
#endif
  
  modint_mat_mul3(m, n, p, c3, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c3"); clock_reset(); clock_start();
#endif

  modint_mat_mul4(m, n, p, c4, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c4"); clock_reset(); clock_start();
#endif
  
  modint_mat_mul5(m, n, p, c5, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c5"); clock_reset(); clock_start();
#endif
    
  modint_mat_mul6(m, n, p, c6, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c6"); clock_reset(); clock_start();
#endif

  modint_mat_mul7(m, n, p, c7, p, a, n, b, p);
#ifdef VERIBENCH
  clock_stop();  printstr_total_clock("c7");
#endif

  printf("c1==c2: %s\n"
	 "c1==c3: %s\n"
	 "c1==c4: %s\n"
	 "c1==c5: %s\n"
	 "c1==c6: %s\n"
	 "c1==c7: %s\n"
	 , modint_mat_equal(m, n, c1, p, c2, p)?"true":"false"
	 , modint_mat_equal(m, n, c1, p, c3, p)?"true":"false"
	 , modint_mat_equal(m, n, c1, p, c4, p)?"true":"false"
	 , modint_mat_equal(m, n, c1, p, c5, p)?"true":"false"
	 , modint_mat_equal(m, n, c1, p, c6, p)?"true":"false"
	 , modint_mat_equal(m, n, c1, p, c7, p)?"true":"false"
	 );
  
  free(a);
  free(b);
  free(c1);
  free(c2);
  free(c3);
  free(c4);
  free(c5);
  free(c6);
  free(c7);
  return 0;
}
