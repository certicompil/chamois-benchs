#include "config.h"
#include "arrays.h"
#include "gmllexer.h"
#include "gmlparser.h"
#include "eval.h"
#ifdef VERIBENCH
#include "veribench.h"
#endif

int main(int argc, char ** argv)
{
  arena_init();
  init_lexer();

#ifdef VERIBENCH
  clock_prepare();
  clock_start();
#endif
  
  execute_program(parse_program());
  
#ifdef VERIBENCH
  clock_stop();
  print_total_clock();
#endif
  
  return 0;
}
