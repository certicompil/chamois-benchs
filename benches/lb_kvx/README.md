# KVX supported benchmarks

This folder contains the list of benchmarks supported by KVX.
We use symbolic links when all benchmarks of a given list are supported.

The following section describes the reason why some benchmarks are currently
unsupported on KVX.

Some other benchmarks are unsupported no matter the architecture.
See ../README.md for an exhaustive list.

## List of lb0 benchmarks unsupported by KVX

### mibench

- qsort: Stack overflow detected
- susan: already in tacle-bench

### tacle-bench

- ammunition, sha, susan: build error, size_t is already defined in KVX headers
- rijndael_enc: nothing is printed on GCC
- rijndael_dec: GCC regression, nothing is printed

### verimag

- raytracer: kvx-jtag-runner timing out. Execute after radio_transmitter to
  reproduce.
- spass: out of memory error

### llvmtest

- ASCI_Purple: "double trap condition detected for ring level 1" followed by 
  timeout
- ASC_Sequoia/AMGmk, ASC_Sequoia/IRSmk, BitBench/drop3, BitBench/uudecode,
  CoyoteBench/huffbench, DOE_ProxyApps_C/XSBench, Misc/himenobmtxpa: same
- CoyoteBench/almabench: timeout (longer than 60 sec)
- FreeBench/pcompress2: out of memory
- mafft: code too big
- McCat/01-qbsort: timeout
- McCat/09-vor: "Can't create node"
- McCat/15-trie: timeout
- McGill/chomp: timeout
- Misc/ffbench: broken printing

