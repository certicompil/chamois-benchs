# Driver Makefile as a wrapper to the buildBin.sh, cleanBenches.sh and
# runBenches.sh scripts

USAGE_MSG="Usage: make (clean|build|run) CONF=conf BENCH=the_bench"

.PHONY: help build run clean all

ifndef BENCH
$(error $(USAGE_MSG))
else
ifndef CONF
$(error $(USAGE_MSG))
else
all: clean build run
endif
endif

run:
	@echo $(BENCH) | ./runBenches.sh $(CONF)

build:
	@echo $(BENCH) | ./buildBin.sh $(CONF)

clean:
	@echo $(BENCH) | ./cleanBenches.sh

help:
	@echo $(USAGE_MSG)

