#include "cycles.h"

#ifndef CYCLE_DISABLE
static cycle_t cumul_cycle, last_cycle_start, last_cycle_stop;
#endif

#ifdef CLOCK_ENABLE
static struct timespec cumul_clkTspec, clkTspecStart, clkTspecStop;

// #include <time.h>
// int clock_gettime(clockid_t clk_id, struct timespec *tp);
// struct timespec {
//   time_t   tv_sec;        /* seconds */
//   long     tv_nsec;       /* nanoseconds */
// };
// The value of the nanoseconds field must be in the range 0 to 999999999.

#define NS_PER_SECOND  1000000000L
#define NS_PER_SECOND_DBL 1000000000.0
#define CLCKID CLOCK_MONOTONIC 

/**
 * @fn timespec_diff(struct timespec *, struct timespec *, struct timespec *)
 * @brief Compute the diff of two timespecs, that is a - b = result.
 * @param a the minuend
 * @param b the subtrahend
 * @param result a - b
 */
static inline void timespec_diffnaif(struct timespec *result,
				     struct timespec *a, struct timespec *b)
{
  result->tv_sec  = a->tv_sec  - b->tv_sec;
  result->tv_nsec = a->tv_nsec - b->tv_nsec;
  if (result->tv_nsec < 0) {
    --result->tv_sec;
    result->tv_nsec += NS_PER_SECOND;
  }
}

/**
 * @fn timespec_diff(struct timespec *, struct timespec *, struct timespec *)
 * @brief Compute the diff of two timespecs, that is x - y = result. (https://www.gnu.org/software/libc/manual/html_node/Calculating-Elapsed-Time.html)
 * @param x the minuend
 * @param y the subtrahend
 * @param result x - y
 * @return 1 if the difference is negative, otherwise 0.
 */
static inline int timespec_diff(struct timespec *result,
				struct timespec *x, struct timespec *y)
{
  /* Perform the carry for the later subtraction by updating y. */
  if (x->tv_nsec < y->tv_nsec) {
    int nsec = (y->tv_nsec - x->tv_nsec) / NS_PER_SECOND + 1;
    y->tv_nsec -= NS_PER_SECOND * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_nsec - y->tv_nsec > NS_PER_SECOND) {
    int nsec = (x->tv_nsec - y->tv_nsec) / NS_PER_SECOND;
    y->tv_nsec += NS_PER_SECOND * nsec;
    y->tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_nsec is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_nsec = x->tv_nsec - y->tv_nsec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}

/**
 * @fn timespec_cumul(struct timespec *, struct timespec *, struct timespec *)
 * @brief Compute the diff of two timespecs, that is x - y = delta and add it to cumul result (https://www.gnu.org/software/libc/manual/html_node/Calculating-Elapsed-Time.html)
 * @param x the minuend
 * @param y the subtrahend
 * @param cumul += (x - y)
 * @return 1 if the difference (x - y) is negative, otherwise 0.
 */
static inline int timespec_cumul(struct timespec *cumul,
				 struct timespec *x, struct timespec *y)
{
  struct timespec diff;
  int result = timespec_diff(&diff, x, y);
  cumul->tv_nsec = (cumul->tv_nsec + diff.tv_nsec) % NS_PER_SECOND;
  cumul->tv_sec +=  (cumul->tv_nsec + diff.tv_nsec) /  NS_PER_SECOND;
  return result;
}
  
double timespec_diffDbl(const struct timespec *time1, const struct timespec *time0) {
  return (time1->tv_sec - time0->tv_sec)
      + (time1->tv_nsec - time0->tv_nsec) /  NS_PER_SECOND_DBL;
}
#endif

uint32_t dm_random_uint32(void) {
  static uint32_t current_dm_random=UINT32_C(0xDEADBEEF);
  current_dm_random = ((uint64_t) current_dm_random << 6) % UINT32_C(4294967291);
  return current_dm_random;
}

void clock_reset(void) {
#ifndef CYCLE_DISABLE
  cumul_cycle = 0;
#endif
#ifdef CLOCK_ENABLE
  cumul_clkTspec.tv_sec = 0; 
  cumul_clkTspec.tv_nsec = 0; 
#endif
}

void clock_prepare(void) {
  clock_reset();
  printf("\n[veribench] clock_prepare:");
#ifndef CYCLE_DISABLE
  cycle_count_config();
  printf(" cycle enable");
#ifdef GET_CYCLE_ALWAYS_RETURN_ZERO
  printf(" (WARNING get_cycle ALWAYS return zero!)");
#endif
  
#endif
#ifdef CLOCK_ENABLE
#ifndef CYCLE_DISABLE
  printf(",");
#endif
  printf(" clock enable");
#endif
  printf("\n");
}

void clock_start(void) {
#ifndef CYCLE_DISABLE
  last_cycle_start = get_cycle();
#endif
#ifdef CLOCK_ENABLE 
  clock_gettime(CLCKID, &clkTspecStart);
#endif
}

void clock_stop(void) {
#ifndef CYCLE_DISABLE
  last_cycle_stop = get_cycle();
  cumul_cycle += last_cycle_stop - last_cycle_start;
#endif
#ifdef CLOCK_ENABLE
  clock_gettime(CLCKID, &clkTspecStop);
  timespec_cumul(&cumul_clkTspec,  &clkTspecStop,  &clkTspecStart);
#endif
}

#ifdef CLOCK_ENABLE
void  printstr_total_clock_timespec(char *s) {
  printf("\n%s clk_ns: %12ju\n", s, NS_PER_SECOND*(cumul_clkTspec.tv_sec) + cumul_clkTspec.tv_nsec);
#ifndef CYCLE_DISABLE
  double  freq = cumul_cycle / ( NS_PER_SECOND_DBL * cumul_clkTspec.tv_sec + cumul_clkTspec.tv_nsec);
  printf("\n%s cycl/clock:  %12.10e\n", s, freq);
#endif
}

void  printerrstr_total_clock_timespec(char *s) {
  fprintf(stderr, "\n%s clk_ns: %12ju\n", s, NS_PER_SECOND*(cumul_clkTspec.tv_sec) + cumul_clkTspec.tv_nsec);
#ifndef CYCLE_DISABLE
  double  freq = cumul_cycle / ( NS_PER_SECOND_DBL * cumul_clkTspec.tv_sec + cumul_clkTspec.tv_nsec);
  fprintf(stderr, "\n%s cycl/clock:  %12.10e\n", s, freq);
#endif
}
#endif

void printstr_total_clock(char *s) {
#ifndef CYCLE_DISABLE
  printf("\n%s cycles: %12" PRcycle "\n", s, cumul_cycle);
#endif
#ifdef CLOCK_ENABLE
  printstr_total_clock_timespec(s);
#endif
}

void print_total_clock(void) {
  printstr_total_clock("time");
}

void printerrstr_total_clock(char *s) {
#ifndef CYCLE_DISABLE
  fprintf(stderr, "\n%s cycles: %12" PRcycle "\n", s, cumul_cycle);
#endif
#ifdef CLOCK_ENABLE
  printerrstr_total_clock_timespec(s);
#endif
}

void printerr_total_clock(void) {
  printerrstr_total_clock("time");
}

void do_nothing(char *s, ...){
}
