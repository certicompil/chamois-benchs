#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>

#ifndef CYCLES_H
#define CYCLES_H

#ifdef CYCLE_DISABLE
#define CLOCK_ENABLE
#endif

#ifdef CLOCK_ENABLE
#include <time.h>
#endif

#ifndef CYCLE_DISABLE
// definition, according to the architecture of the host machine, for:
// cycle_t,  PRcycle, void cycle_count_config(void), cycle_t get_cycle(void) 
// usualy :
// - cycle_t uint64_t or  uint32_ for 32bits arch.
// - PRcycle is defined as PRId64 
//  ( printf("My value is %10" PRId64 "\n", some_64_bit_expression); )
//


//#define RISCV_NO_PRIVILEGE

// --------------------------------------------------------------------- __KVX__
#ifdef __KVX__
typedef uint64_t cycle_t;
#define PRcycle PRId64

// Values taken from $(TOOLCHAIN)/include/hal/cos_registers.h
#define COS_SFR_PMC 12
#define COS_SFR_PM0 13

static inline void cycle_count_config(void)
{
        /* config pmc for cycle count */
        cycle_t pmc_value = __builtin_kvx_get(COS_SFR_PMC);

        pmc_value &= ~(0xfULL);
        __builtin_kvx_set(COS_SFR_PMC, pmc_value);
}

static inline cycle_t get_cycle(void)
{
        return __builtin_kvx_get(COS_SFR_PM0);
}

// ----------------------------------------------------------------- NOT __KVX__
#else // ! #ifdef __KVX__

static inline void cycle_count_config(void) { }

// ------------------------------------------------------------------ __x86_64__
#if defined( __x86_64__)

#define PRcycle PRId64
typedef uint64_t cycle_t;

static inline cycle_t get_cycle(void)
{
  cycle_t tsc;
  __asm__ __volatile__(
		       "rdtscp;"
		       "shl $32, %%rdx;"
		       "or %%rdx, %%rax;"
		       "movq %%rax, %0"
		       : "=r"(tsc)
		       :
		       : "rax", "rcx", "rdx");
  return tsc;
}

// --------------------------------------------------------- __i386__ __x86_64__
#elif defined(__i386__) || defined( __x86_64__) 

#define PRcycle PRId64
typedef uint64_t cycle_t;
#include <x86intrin.h>
static inline cycle_t get_cycle(void) { return __rdtsc(); }


// --------------------------------------------------------------------- __riscv
#elif __riscv

#if __riscv_xlen==32
#define PRcycle PRId32
typedef uint32_t cycle_t;
#else
#define PRcycle PRId64
typedef uint64_t cycle_t;
#endif // if __riscv_xlen==32

#ifdef RISCV_NO_PRIVILEGE
static inline cycle_t get_cycle(void) { return 0; }
#define GET_CYCLE_ALWAYS_RETURN_ZERO

#else
static inline cycle_t get_cycle(void) {
  cycle_t cycles;
  asm volatile ("rdcycle %0" : "=r" (cycles));
  return cycles;
}
#endif // ifdef RISCV_NO_PRIVILEGE

// ------------------------------------------------------------------ __ARM_ARCH
#elif defined (__ARM_ARCH) // && (__ARM_ARCH >= 6)
#if (__ARM_ARCH < 8)
typedef uint32_t cycle_t;
#define PRcycle PRId32

#ifdef ARM_NO_PRIVILEGE
static inline cycle_t get_cycle(void) { return 0; }
#define GET_CYCLE_ALWAYS_RETURN_ZERO

#else
/* need this kernel module
https://github.com/zertyz/MTL/tree/master/cpp/time/kernel/arm */
static inline cycle_t get_cycle(void) {
  cycle_t cycles;
  __asm__ volatile ("mrc p15, 0, %0, c9, c13, 0":"=r" (cycles));
  return cycles;
}
#endif // ifdef ARM_NO_PRIVILEGE

#else // ! __ARM_ARCH < 8
#define PRcycle PRId64
typedef uint64_t cycle_t;

  #ifdef ARM_NO_PRIVILEGE
static inline cycle_t get_cycle(void) { return 0; }
#define GET_CYCLE_ALWAYS_RETURN_ZERO

#else // ! ARM_NO_PRIVILEGE
/* need this kernel module:
https://github.com/jerinjacobk/armv8_pmu_cycle_counter_el0

on 5+ kernels, remove first argument of access_ok macro */
static inline cycle_t get_cycle(void)
{
  uint64_t val;
  __asm__ volatile("mrs %0, pmccntr_el0" : "=r"(val));
  return val;
}
#endif // ifdef ARM_NO_PRIVILEG

#endif // if (__ARM_ARCH < 8)

#else
#define PRcycle PRId32
typedef uint32_t cycle_t;
static inline cycle_t get_cycle(void) { return 0; }
#define GET_CYCLE_ALWAYS_RETURN_ZERO

#endif // elif defined (__ARM_ARCH)

#endif // ifdef __KVX__

#endif // ifndef CYCLE_DISABLE

#endif // ifndef CYCLES_H
