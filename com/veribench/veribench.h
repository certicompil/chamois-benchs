#ifndef DEF_VERIBENCH_H 
#define DEF_VERIBENCH_H
 
#include <stdint.h>
#include <limits.h>

//OL see benches/lustre
#define LOOPC 3000

#if __WORDSIZE == 32
typedef uint32_t cycle_t;
#define PRcycle PRId32
#else
typedef uint64_t cycle_t;
#define PRcycle PRId64
#endif

void clock_prepare(void);
void clock_reset(void);
void clock_start(void);
void clock_stop(void);
void print_total_clock(void);
void printstr_total_clock(char *);
void printerr_total_clock(void);

uint32_t dm_random_uint32(void);

void do_nothing(char *s, ...);

#endif // ifndef DEF_VERIBENCH_H
