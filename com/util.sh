#!/usr/bin/env bash
#


# styles ansi escape codes
reset="\x1B[0m"
bold="\x1B[1m"
faint="\x1B[2m"
italic="\x1B[3m"
underline="\x1B[4m"
inverse="\x1B[7m"
strike="\x1B[9m"
# colors ansi escape codes
black="\x1B[30m"
red="\x1B[31m"
green="\x1B[32m"
yellow="\x1B[33m"
blue="\x1B[34m"
magenta="\x1B[35m"
cyan="\x1B[36m"
white="\x1B[37m"


readonly TRACE_COLOR=$green
readonly TRACE_ON=1   # trace on 
# readonly TRACE_ON=0 # trace off


readonly damnArr=(Crap Damn Heck Zut Drats Tut Blast Skit)

readonly greatArr=(Awesome Breathtaking Amazing Stunning Astounding Astonishing Fantastic Stupendous Staggering Extraordinary Incredible Unbelievable Magnificent Wonderful Spectacular Remarkable Phenomenal Prodigious Miraculous Sublime Formidable Imposing Impressive)
# greatArrSize=${#greatArr[*]}




#-------------------------------------------------------------------------------
#--------------------------------------------------------------------- functions
#


# $1 : string to echo
# $2 : color option (can be concat with style codes)
# usage echoC "blabla" $red$underline
function util.echoC () {
  echo -e "$2$1${reset}"
}


function util.getRdGreatMsg () {
  let "index=$RANDOM % ${#greatArr[*]}"
  echo "${greatArr[$index]}!"
}


function util.getRdDamnMsg () {
  let "index=$RANDOM % ${#damnArr[*]}"
  echo "${damnArr[$index]}!"
}


# $1 : string to echo
function util.trace () {
  (( TRACE_ON )) && util.echoC "[TRACE] $1" $TRACE_COLOR
}
# util.trace "(util.sh:$LINENO in $FUNCNAME) blabla"


function util.testPrefix () {
  local pf
  local sf
  [ "x$1" = x ] && return 1
  [ "x$2" = x ] && return 1

  pf=$1         # prefix
  sf="${2#$pf}" # suffix
  [ x"$pf$sf" = x"$2" ] && return 0

  return 1
}


function util.testTool () {
  local r

  which $1 > /dev/null 2>&1
  r=$?
  if [ $r == 1 ]; then
    echo -e  >&2 "command '$1' not found but it's required.\n $2"
  fi
  return $r
}


function util.getGitInfo () {
  GIT_REV="N/A"
  GIT_DIFF=""
  GIT_BRANCH="N/A"
  GIT_TAG="N/A"
  local r
  local tt

  which git > /dev/null 2>&1
  r=$?
  # util.trace "(util.sh:$LINENO in $FUNCNAME) r=$r"
  if [ $r == 0 ]; then # tool git is present
    # ------------- git revision
    tt=$(git log --pretty=format:'%h' -n 1 2>/dev/null)
    r=$?
    if [ $r == 0 ]; then # in a git repository
      GIT_REV=$tt

      # ------------- git diff ('+' ou  '')
      # '+' if there are uncommitted changes.
      tt=$(git diff --quiet --exit-code || echo + )
      r=$?
      if [ $r == 0 ]; then
        GIT_DIFF=$tt
      fi

      # ------------- git tag
      tt=$(git describe --exact-match --tags 2>/dev/null)
      r=$?
      if [ $r == 0 ]; then
        GIT_TAG=$tt
      fi

      # ------------- git branch
      tt=$(git branch --show-current 2>/dev/null)
      r=$?
      if [ $r == 0 ]; then
        GIT_BRANCH=$tt
      fi
    fi
    # util.trace "(util.sh:$LINENO in $FUNCNAME) GIT_BRANCH=${GIT_BRANCH}, GIT_REV=${GIT_REV}${GIT_DIFF}, GIT_TAG=${GIT_TAG}"
  fi
  return $r
}




# Output variables:
#  - style: reset, bold, faint, italic, underline, inverse strike;
#  - colors: black; red, gree, yellow, blue, magenta, cyan; white
#  - GIT_BRANCH, GIT_REV, GIT_DIFF, GIT_TAG
# Functions:
# - util.echoC(str, color)
# - util.getRdGreatMsg()
# - util.getRdDamnMsg()
# - util.trace(str)
# - util.testPrefix(str str)
# - util.gettestTool(str str)
# - util.getGitInfo ()
# ----------------------------------------- eof util.sh
#_____
