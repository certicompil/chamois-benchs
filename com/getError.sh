#!/usr/bin/env bash

VERIBENCH_ROOT=$(pwd)
BENCHES_ROOT=$VERIBENCH_ROOT/benches
source $VERIBENCH_ROOT/com/util.sh      # definition echoC, blue, magenta ...
source $VERIBENCH_ROOT/selectBenches.sh > /dev/null # output: $simubenches, $hardbenches, $benches

# the list of benches selected via "selectBenches.sh" script 
BENCH_LIST=$benches


FILENAME="compileerr.log"

# format output echo 
SP="#"

for bench in $BENCH_LIST; do
    if [ ! -d "$BENCHES_ROOT/$bench" ]; then
        echoC "$SP$SP$SP $bench not found!" $red
    else
	if [ -f $BENCHES_ROOT/$bench/$FILENAME ]; then
	    FILESIZE=$(wc -l < "$BENCHES_ROOT/$bench/$FILENAME" | tr -dc '0-9' )
	    if [ $FILESIZE \> 0 ]; then
		# echoC "$SP $bench, $FILESIZE" $yellow
		echo "$bench"
	    fi
	fi
    fi
done


