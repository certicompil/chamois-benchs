#!/usr/bin/env bash
#

# This script defines several definitions and functionalities for
# buildBin.sh, cleanBenches.sh and runBenches.sh scripts :
# - define some utilities (via util.sh)
# - check the availibity of required tools
# - define VERIBENCH_ROOT
# - define DEFAULT_BENCHESF the benches "root" folder
# - define listAbsPath2Bench the list of absolute path to benches
# The list of path is built from text file, input parameter
# or from stdin


#------------------------------------------------------ veribench root directory
#
# The path VERIBENCH_ROOT is needed in local bench Makefile
# to include rules.mk
#
# the default path should be where build and run script are defined
#
# if VERIBENCH_ROOT is unset or set to the empty string
# then set to the current/working directory
if [ -z ${VERIBENCH_ROOT+x} ]; then
  VERIBENCH_ROOT=$(pwd)
  export VERIBENCH_ROOT
fi
readonly VERIBENCH_ROOT


#---  some utilities (util.trace, util.echoC, blue, magenta ...)
source com/util.sh


#--------------------------------------------------- (default) benches directory
#
DEFAULT_BENCHESF="benches"
readonly DEFAULT_BENCHESF

# e.g. export VERIBENCH_BENCHESF="/home/toto/my/benches"
#
# if  VERIBENCH_BENCHESF its unset or set to the empty string:
# set it to $VERIBENCH_ROOT/$DEFAULT_BENCHES
if [ -z ${VERIBENCH_BENCHESF+x} ]; then
  VERIBENCH_BENCHESF=$VERIBENCH_ROOT/$DEFAULT_BENCHESF
  export VERIBENCH_BENCHESF
fi

# util.trace "VERIBENCH_BENCHESF=$VERIBENCH_BENCHESF"
readonly VERIBENCH_BENCHESF


#------------------------------------------------------ check tools availability
#
readonly testAvailabilityRequiredTool="ON"

#---------------- collect and centralize errors during build or run in this file
#
readonly preGLOBAL_ERROR_LOG_FILE=$VERIBENCH_ROOT/globalError.log
export preGLOBAL_ERROR_LOG_FILE
rm -f $preGLOBAL_ERROR_LOG_FILE


#------- keep trace of number of input path of bench, and number of invalid path
#
preNbInputPath=0
preNbInvalidPath=0


#--------------------------------------------------------------- list of benches
#
# list of absolute paths of benches
listAbsPath2Bench=""



#-------------------------------------------------------------------------------
#--------------------------------------------------------------------- functions
#

# ----------------------------
# test number of invalidPath > 0
# ( $preNbInputPath $preNbInvalidPath )
function pre.traceInvalidPath () {
  if [[ $preNbInvalidPath -ne 0 ]]; then
    echo "------------------------------"
    util.echoC "=/ $(util.getRdDamnMsg) #nbInputPath=$preNbInputPath #nbInvalidPath=$preNbInvalidPath" $red
    echo "------------------------------"
  fi
}


#######################################
# error msg
# Globals:
#  preGLOBAL_ERROR_LOG_FILE (used, unmodified)
#  red (used, unmodified)
# Arguments:
#   $* error msg(s)
# Outputs:
#  Output to STDOUT,
#  concat in preGLOBAL_ERROR_LOG_FILE
######################################
pre.errmsg() {
  local errStr="[ERROR]$*"
  util.echoC "$errStr" $red
  echo  $errStr >> $preGLOBAL_ERROR_LOG_FILE
}



#######################################
# get list of bench path from text file
# path are relative to VERIBENCH_BENCHES
# or are absolute path 
# Globals:
#   VERIBENCH_BENCHESF (used, unmodified)
#   output: preNbInputPath, preNbInvalidPath
#   output: listAbsPath2Benchh
# Arguments:
#   $1 the text file
######################################
function fromTextFile() {
  local partialList="$(<$1)"
  local bb
  local bbi=1
  for bb in $partialList; do
    ((preNbInputPath++))
    # check if this path (relatif to $VERIBENCH_BENCHES) is a directory
    if [ -d "$VERIBENCH_BENCHESF/$bb" ]; then
      bb=$VERIBENCH_BENCHESF/$bb
      absPath=$(echo "$(cd "$(dirname "$bb")"; pwd)/$(basename "$bb")")
      listAbsPath2Bench+=" $absPath"
    elif  [ -d "$bb" ]; then
      absPath=$(echo "$(cd "$(dirname "$bb")"; pwd)/$(basename "$bb")")
      listAbsPath2Bench+=" $absPath"
    else
      ((preNbInvalidPath++))
      pre.errmsg "[INPUT] in file '$1', line $bbi, the entry '$bb' is not a directory, input ignored."
    fi
    ((bbi++))
  done
}





#-------------------------------------------------------------------------- main
#
#

readonly jqTool='jq'
readonly jqTool_info='jq is a lightweight and flexible command-line JSON processor (https://stedolan.github.io/jq/).'
if [ ! -z ${testAvailabilityRequiredTool+x} ]; then
  util.testTool "$jqTool" "$jqTool_info"
  if [ $? != 0 ] ; then
    echo "One or several required tools are missing or too old.  Aborting."
    exit 1
  fi
fi


# ------------------------------------------------------------------------- 1st:
# input parmaters shoud be either text file or path
# Input text files: contains liste bench defined by their
# path relative to VERIBENCH_BENCHESF or absolute path

i=0
while (( "$#" )); do
  ((i++))
  # util.trace "arg$i: $1"

  # if input arg. is a file, then consider it
  # as a text file containing list of path
  if [ -f $1 ]; then
    # fromTextFile "$(<$1)"
    fromTextFile $1

    # elif is a directory then add it to list
  elif [ -d $1 ]; then
    ((preNbInputPath++))
    if [ $i -gt 0 ]; then listAbsPath2Bench+=" "; fi
    # TODO 2REMOVE   if [ $i -gt 0 ]; then listRelPath2Bench+=" "; fi
    absPath=$(echo "$(cd "$(dirname "$1")"; pwd)/$(basename "$1")")
    listAbsPath2Bench+=" $absPath"
    # TODO 2REMOVE   listRelPath2Bench+=" $1"
  elif [ "$1" != "csvclean" ] && [ "$1" != "binclean" ]; then
    ((preNbInputPath++))
    ((preNbInvalidPath++))
    pre.errmsg "[INPUT] argument '$1' is neither a valid directory or valid file, input ignored"
  fi
  shift
done



# -------------------------- 2nd: if nothing past via parameters then read stdin
if [ $i -eq 0 ]; then
  j=0
  while read line
  do
    ((j++))
    for bench in $line; do
      ((preNbInputPath++))
      if [ -d $VERIBENCH_BENCHESF/$bench ]; then
        bb=$VERIBENCH_BENCHESF/$bench
        if [ $j -gt 0 ]; then listAbsPath2Bench+=" "; fi
        # TODO 2REMOVE   if [ $j -gt 0 ]; then listRelPath2Bench+=" "; fi
        absPath=$(echo "$(cd "$(dirname "$bb")"; pwd)/$(basename "$bb")")
        # TODO 2REMOVE   relPath=$bb
        listAbsPath2Bench+="$absPath"
        # TODO 2REMOVE   listRelPath2Bench+="$relPath"
      elif [ -d $bench ]; then
        if [ $j -gt 0 ]; then listAbsPath2Bench+=" "; fi
        # TODO 2REMOVE   if [ $j -gt 0 ]; then listRelPath2Bench+=" "; fi
        absPath=$(echo "$(cd "$(dirname "$bench")"; pwd)/$(basename "$bench")")
        # TODO 2REMOVE   relPath=$bench
        listAbsPath2Bench+="$absPath"
        # TODO 2REMOVE   listRelPath2Bench+="$relPath"
      elif  [ -f $bench ]; then
        fromTextFile $bench
      else
        ((preNbInvalidPath++))
        pre.errmsg "[INPUT] $bench is not a valid directory, input ignored."
      fi
    done
  done # < /dev/stdin
fi


readonly listAbsPath2Bench
readonly preNbInputPath
readonly preNbInvalidPath

# Output variables: VERIBENCH_ROOT, VERIBENCH_BENCHESF, listAbsPath2Benc, preNbInvalidPath
# Functions: pre.traceInvalidPath()
#
# ----------------------------------------- eof preliminary.sh
#_____
