#!/usr/bin/env bash
#

#
# ----------------------------------------- begin
#
scrptnm=$(basename $0)
# echo "scrptnm='$scrptnm', arg1='$1'"



listDirectory=""

#
# ----------------------------------------- usage and parameter
#
# check at least 1 argument (path to a folder)

if [ "$#" -lt 1 ]; then
    echo "Usage: $scrptnm DIRECTORY..." >&2
else    
i=0
while (( "$#" )); do
    ((i++))
    if  [ -d "$1" ]; then
	echo  "$scrptnm:  $1 added"	
	listDirectory+=" $1"
    else
	echo  "$scrptnm:  '$1': No such directory, ignored." >&2	
    fi
    shift 
done
fi


if [ -z "$listDirectory" ]; then
    echo "nothing to do."
    exit 1
fi


for dd in $listDirectory; do
    echo "in directory '$dd'"
    lddb=$(ls -d benches/llvmtest/Misc/*/)
    for ddb in $lddb; do
	if  [ -f "$ddb/Makefile" ]; then
	    if [ ! -f  "$ddb/Makefile.origin" ]; then
		mv "$ddb/Makefile" "$ddb/Makefile.origin"
	    fi
	fi
	target=$(basename $ddb)
	echo "target='$target'"
	printf "TARGET=$target

" > "$ddb/Makefile"
	printf "include \$(VERIBENCH_ROOT)/rules.mk
" >> "$ddb/Makefile"
    done
done



# ----------------------------------------- eof buildBench.sh
#_____
