#!/usr/bin/env bash

# usage ex. : com/getError.sh | com/logError.sh | more

VERIBENCH_ROOT=$(pwd)
BENCHES_ROOT=$VERIBENCH_ROOT/benches

# collect and centralize all errors during build in this file:
# GLOBAL_BUILD_ERROR_LOG_FILE=$VERIBENCH_ROOT/benches/globalBuildError.log
# gbelf=$GLOBAL_BUILD_ERROR_LOG_FILE
# rm -f $gbelf

LOGFILENAME="compileerr.log"

while IFS= read -r line;
do
    echo "# ----------<  $line"
    echo  $BENCHES_ROOT/$line/$LOGFILENAME
    if [ -f $BENCHES_ROOT/$line/$LOGFILENAME ]; then
	cat $BENCHES_ROOT/$line/$LOGFILENAME
    fi
    echo "# $line >----------" 
done



