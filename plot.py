"""
A python script to analyze benchmark results.

Author: Léo Gourdin, VERIMAG, UGA

Feel free to modify this code.
"""

# import math
import os, sys

import json
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas import DataFrame
from matplotlib.ticker import LogLocator, StrMethodFormatter
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("config", help="JSON config file", type=str)
parser.add_argument("ref", help="reference column", type=str)
parser.add_argument("--size", help="plot program size", action="store_true")
parser.add_argument("--latex", help="save results as latex", action="store_true")
parser.add_argument("--md", help="save results as markdown", action="store_true")
parser.add_argument("--txt", help="save results as text", action="store_true")
parser.add_argument("--plot", help="save results as plots", action="store_true")
parser.add_argument("--nruns", help="number of runs", type=int, default=1)
parser.add_argument(
    "--rsd",
    help="Relative Standard Deviation threshold (in %%)",
    type=float,
    default=float("inf"),
)
parser.add_argument("--input-filter", help="benches to eliminate", type=str)
parser.add_argument("--split", help="split wrt either <csv file> or prefixes", type=str)
args = parser.parse_args()

PREFIX = "measures_cycles"
# RSD threshold
THRESHOLD = args.rsd
# Output folder
FOUTPUT = "plot_output"
# The output file for plot
PLOT_OUTPUT = "plot_output"
# Output list of RSD-eliminated benches
ELIM_OUTPUT = "eliminated.csv"
# LaTeX output file (formatted in array)
LATEX_OUTPUT = "latex_res.tex"
# Markdown output file (formatted in array)
MD_OUTPUT = "tbl_res.md"
# Columns to consider
CCOMP = []

PP_MD = None
PP_TEX = None
PP_TXT = None

# Reference column (the one to compare with)
REF = " " + args.ref
# Best column, to be compared with gcc
BEST = " gcc.O2"

DPTAG = "_kdp"


# Convert column to floats
def convert_columns(df):
    # df = df[~df.index.duplicated()].copy()
    df.replace("", np.nan, inplace=True)
    df.replace(" ", np.nan, inplace=True)
    df.dropna(inplace=True)
    for col in df.columns:
        df[col] = df[col].astype(float)
    return df


# Eliminate benches according to the RSD
def eliminate_benches(dfs, rsd_dict):
    if not args.input_filter:
        with open(FOUTPUT + "/" + ELIM_OUTPUT, "w") as ef:
            for k, v in rsd_dict.items():
                mean = np.mean(v)
                std = np.std(v)
                rsd = 100 * (std / mean)
                if rsd > THRESHOLD:
                    print("Bench", k, "is too much derivating and will be removed")
                    ef.write(k + "\n")
                    dfs = [df.drop(k, errors="ignore") for df in dfs]
    else:
        with open(args.input_filter, "r") as inf:
            lines = inf.readlines()
            for line in lines:
                k = line.strip("\n")
                print("Bench", k, "is in the filter and will be removed")
                dfs = [df.drop(k, errors="ignore") for df in dfs]
    return dfs


def get_prefix(row):
    sprefix = row.split("/")
    if len(sprefix) == 1:
        raise Exception("Option --split prefix was given but there is no prefix on line " + str(i+1))
    return sprefix[0]


# Split dataframes according to intervals file
def split_csv(df):
    dflen = len (df[REF]) - 1
    groups = [["all", 0, dflen]]
    if args.split:
        if args.split.endswith(".csv"):
            splitfile = open(args.split, "r")
            split_csv = splitfile.read()
            splitfile.close()
            split_csv = split_csv.split("\n")[:-1]
            groups.extend([interval.split(",") for interval in split_csv])
        elif args.split == "prefix":
            start = 0
            for i in range(0,dflen+1):
                row = df.index[i]
                prefix = get_prefix(row)
                # print(prefix, row, dflen)
                next_prefix = get_prefix(df.index[i+1]) if i < dflen else ""
                if prefix != next_prefix:
                    groups.append([prefix, start, i])
                    start = i+1
        else:
            print("Warning: unrecognized --split option, no split will be applied.")
    print("Groups:", groups)
    dfs = []
    for group in groups:
        dfs.append(df.iloc[int(group[1]) : int(group[2]) + 1])
    return list(map(lambda g: g[0], groups)), dfs


def remove_trailing_spaces(df):
    return df.rename(index=str.rstrip)


def rename_duplicates(df):
    df.index = df.index + DPTAG + df.groupby(level=0).cumcount().astype(str)
    return df


def read_benches_csv(filename):
    # Read the file, cut according to the bounds, convert columns to float type
    # Remove trailing whitespaces, and rename duplicated columns
    df = pd.read_csv(filename, index_col=0)
    df = convert_columns(df)
    df = remove_trailing_spaces(df)
    return rename_duplicates(df)


# Populate a rsd_dict used to compte rsd
def populate_dict(df, rsd_dict):
    for rid, row in df.iterrows():
        v = []
        if rid in rsd_dict:
            v = rsd_dict[rid]
        v.append(row[REF])
        rsd_dict[rid] = v
    return rsd_dict


# Prepare CSV and gain columns
def prepare_data():
    # Create a new dataframe
    df = None
    rsd_dict = {}
    # Parsing data, filtering, and computing the mean
    # If there is only one run
    if args.nruns == 1:
        basename = PREFIX + "_size" if args.size else PREFIX
        df = read_benches_csv(basename + ".csv")
    else:
        # Prepare an empty dict for computing the RSD
        N = args.nruns
        PATH = "res/" + PREFIX + ".run"
        # For each run result csv...
        for i in range(1, N + 1):
            dft = read_benches_csv(PATH + str(i) + ".csv")
            # Update the RSD dictionary with the current results
            rsd_dict = populate_dict(dft, rsd_dict)
            # Then, for each column, add with the main dataframe
            for col in CCOMP:
                if i == 1:
                    df = dft
                else:
                    df[col] += dft[col]

        # Divide to obtain the mean
        for col in CCOMP:
            df[col] /= N

    dfs_names, dfs = split_csv(df)
    # Apply filter (either RSD or given file)
    dfs = (
        eliminate_benches(dfs, rsd_dict) if args.nruns > 1 or args.input_filter else dfs
    )

    return dfs_names, dfs


# Compute quantiles for a given column
def get_quantiles(col):
    quants = col.quantile([0.25, 0.5, 0.75])
    q1 = f"{quants[0.25]:+.2f}\\%"
    q2 = f"{quants[0.5]:+.2f}\\%"
    q3 = f"{quants[0.75]:+.2f}\\%"
    return quants, q1, q2, q3


def print_stats(df, lcol, lref):
    prec = 2
    bmin, pmin = (df[lcol].idxmin(), str(round(df[lcol].min(), prec)))
    bmax, pmax = (df[lcol].idxmax(), str(round(df[lcol].max(), prec)))
    pmed = str(round(df[lcol].median(), prec))
    quants, q1, q2, q3 = get_quantiles(df[lcol])
    pmean = str(round(df[lcol].mean(), prec))

    if PP_TXT:
        print("Result for column", lcol, "wrt.", lref, file=PP_TXT)
        print(
            "\tmin: {0} % hit for benchmark {1}".format(pmin, bmin),
            file=PP_TXT,
        )
        print(
            "\tmax: {0} % hit for benchmark {1}".format(pmax, bmax),
            file=PP_TXT,
        )
        print("\tmed: {0} %".format(pmed), file=PP_TXT)
        print(
            "\tquants: [{}]".format(str(["{0:.2f} %, ".format(q) for q in quants])),
            file=PP_TXT,
        )
        # quants_rk = pd.qcut(df[lcol], 4, labels=["Q1", "Q2", "Q3", "Q4"])
        # print(quants_rk)
        print("\tavg: {0} %".format(pmean), file=PP_TXT)
    if PP_MD:
        PP_MD.write(" " + pmean + " (avg) / " + pmed + " (med) |")

    # rf.write(
    # lcol.split("/", maxsplit=1)[1] + " & " + q1 + " & " + q2 + " & " + q3 + "\n"
    # )


def analyse_dataframe(df_name, df, lcols, lref):
    if PP_MD:
        PP_MD.write(df_name + " |")
    # For every considered column according the CCOMP constant
    for i in range(len(CCOMP)):
        lcol, col = (lcols[i], CCOMP[i])
        # Compute the gain wrt REF
        df[lcol] = ((df[REF] - df[col]) / df[col]) * 100
        print_stats(df, lcol, lref)

    if PP_MD:
        PP_MD.write("\n")


def subdivide_interv(inf, sup, n):
    return [inf + k * (sup - inf) / n for k in range(n)]


def generate_file(df_name, df, lcols):
    # Nb cols
    reflen = len(df[REF])
    ind = np.arange(reflen)
    width = 1 / (len(lcols) + 0.5)

    start_inds = subdivide_interv(
        ind + width / 2, ind + len(lcols) * width + width / 2, len(lcols)
    )
    edgecolor = [1, 1, 1]
    linewidth = 1

    fig, ax = plt.subplots(nrows=1, ncols=1)
    rects = []
    for i, column in enumerate(lcols):
        rects.append(
            ax.bar(
                start_inds[i],
                df[column],
                width,
                label=column,
                bottom=0,
                alpha=0.7,
                edgecolor=edgecolor,
                linewidth=linewidth,
            )
        )

    linear = True
    # Y axis
    if not linear:
        ax.set_yscale("symlog")
        ax.yaxis.set_minor_locator(LogLocator(base=10, subs="all"))
    ax.yaxis.set_major_formatter(StrMethodFormatter("{x:.2f}%"))
    # max_height = math.ceil(max([max(df[column]) for column in lcols]))
    # min_height = math.floor(min([min(df[column]) for column in lcols]))
    # yticks = np.linspace(min_height, max_height, num=int((max_height-min_height)/10))
    # print(yticks)
    plt.yticks(size=7.0)

    # X axis
    # tlimit = 50
    # new_labels = lcols
    # new_labels = [i.get_text()[0:tlimit] if len(i.get_text()) > tlimit else i.get_text()
    # for i in ax.xaxis.get_ticklabels()]
    ax.xaxis.grid(True, linestyle="-.")
    ax.yaxis.grid(True, linestyle="--")
    plt.xticks(size=7.0)
    ax.set_xticks(ind)
    ax.set_xticklabels(df.rename(index=lambda s: s.rsplit(DPTAG, 1)[0].rsplit("/", 1)[-1]).index)
    ax.legend()

    plt.setp(ax.get_xticklabels(), rotation=80, horizontalalignment="right")
    fig.set_size_inches(14, 7)
    # plt.tight_layout()
    if args.size:
        plt.title("Program size relatively to " + REF)
    else:
        plt.title("Speed relatively to " + REF)

    name_suffix = "." + df_name + "."
    if args.plot:
        plt.savefig(
            FOUTPUT + "/" + PLOT_OUTPUT + name_suffix + "svg",
            bbox_inches="tight",
            format="svg",
        )
    df.loc["MEAN"] = df.mean()
    df.to_csv(FOUTPUT + "/measures.simu.pd" + name_suffix + "csv")
    # plt.show()


def clean_ccol(c):
    return (
        c.replace(" ccomp.", "CCOMP/")
        .replace(" gcc.", "GCC/")
        .replace(" vanilla_ccomp.", "VANILLA_CCOMP/")
        .replace(" clang.", "CLANG/")
        .replace(" ccomp_", "CCOMP/")
    )


def clean_ccols():
    lcols = []
    for col in CCOMP:
        lcols.append(clean_ccol(col))
    return lcols


def init(lcols, lref):
    global PP_MD
    PP_MD = open(FOUTPUT + "/" + MD_OUTPUT, "w") if args.md else None
    # PP_TEX = open(FOUTPUT + "/" + LATEX_OUTPUT, "w") if args.latex else None

    if PP_MD:
        PP_MD.write("## Results w.r.t. " + lref + "\n\n")
        PP_MD.write("| Compiler/BenchSuite |")
        mdhead = "| - |"
        for lcol in lcols:
            PP_MD.write(" " + lcol + " |")
            mdhead = mdhead + " - |"
        PP_MD.write("\n" + mdhead + "\n")


def pre():
    global CCOMP
    with open(args.config) as jconf:
        data = json.load(jconf)
        for comp in data['benchesComp']:
            prefix = None
            if "gcc" in comp['cc']:
                prefix = "gcc"
            elif "ccomp" == comp['cc']:
                prefix = "ccomp"
            elif "vanilla" in comp['cc']:
                prefix = "vanilla_ccomp"
            elif "clang" in comp['cc']:
                prefix = "clang"
            else:
                prefix = comp['cc']
            if prefix:
                for opt in comp['options']:
                    CCOMP.append(" " + prefix + "." + opt['prefix'])

def finalize():
    if PP_MD:
        PP_MD.close()
    if PP_TEX:
        PP_TEX.close()


def main():
    global PP_TXT

    if args.latex:
        raise Exception("Error: latex generation not supported in this version.")
    if not args.md and not args.txt and not args.plot:
        print("Neither --md, --txt, nor --plot provided. Will only generate csvs.")

    if not os.path.isdir(FOUTPUT):
        os.mkdir(FOUTPUT)
    pre()
    dfs_names, dfs = prepare_data()
    lcols = clean_ccols()
    lref = clean_ccol(REF)

    init(lcols, lref)

    i = 0
    for i in range(len(dfs)):
        df_name, df = (dfs_names[i], dfs[i])
        PP_TXT = open(FOUTPUT + "/" + df_name + ".txt", "w") if args.txt else None
        analyse_dataframe(df_name, df, lcols, lref)
        if PP_TXT:
            PP_TXT.close()
        generate_file(df_name, df, lcols)
        i += 1

    finalize()

if __name__ == "__main__":
    main()
