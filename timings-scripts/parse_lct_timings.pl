#!/usr/bin/perl
use strict;
use warnings;
use Statistics::Basic qw(:all nofill);
use Carp::Assert;
use v5.34;

if (@ARGV < 1) { die "usage: ./parse_timings.pl <lb.txt>" }

open(my $lb, "<", $ARGV[0]);
my $i = 0; my @benches;
my @lct; my @ninsts; my @nblks; my @nlctst; my @se;
while (<$lb>) {
  chomp;
  open(my $log, "<", "../benches/$_/compileerr.log");
  push @benches, $_;
  while (<$log>) {
    if (/^LCT, (.*)$/) {
      $lct[$i] += $1;
    } elsif (/^SZ \(.*=(.*)\/.*=(.*)\)/) {
      $ninsts[$i] += $1;
      $nblks[$i] += $2;
    } elsif (/^Stats: (.*), \d+\.\d+$/) {
      # A line is notrap, ni_notrap, xi_notrap, nr_notrap, xr_notrap, trap, ni_trap, xi_trap, nr_trap, xr_trap, sr, ni_sr, xi_sr, nr_sr, xr_sr, u_sr, nb_aseq_ginv, nb_aseq_hinv
      my @line = split /, /, $1;
      for my $j ( 0 .. $#line) {
        $nlctst[$i][$j] += $line[$j];
      }
    } elsif (/^SE, (.*)$/) {
      $se[$i] += $1;
    }
  }
  $i++;
}

assert($#lct == $#ninsts && $#lct == $#nblks && $#lct == $#nlctst && $#lct == $#se);

my @ais; my @size;
open(my $csv, ">", "timings.csv");
say $csv "Bench,LCT,nb_insts,nb_blks,nb_aseq_gis,nb_aseq_his,nb_aseq_is,size,SE";
for (my $i = 0; $i <= $#lct; $i++) {
  my $lais = $nlctst[$i][16] + $nlctst[$i][17];
  my $lsize = $lais + $ninsts[$i];
  push @ais, $lais;
  push @size, $lsize;
  say $csv "$benches[$i],$lct[$i],$ninsts[$i],$nblks[$i],$nlctst[$i][16],$nlctst[$i][17],$lais,$lsize,$se[$i]";
}
close $csv or die "$csv: $!";
say "Nb benchs=$i";
my $cor_LCT_insts = corr(\@lct,\@ninsts) * 100;
my $cor_LCT_blks = corr(\@lct,\@nblks) * 100;
my $cor_SE_insts = corr(\@se,\@ninsts) * 100;
my $cor_SE_blks = corr(\@se,\@nblks) * 100;
my $cor_LCT_agis = corr(\@lct,[map { $$_[16] } @nlctst]) * 100;
my $cor_LCT_ahis = corr(\@lct,[map { $$_[17] } @nlctst]) * 100;
my $cor_LCT_ais = corr(\@lct,\@ais) * 100;
my $cor_SE_ais = corr(\@se,\@ais) * 100;
my $cor_LCT_size = corr(\@lct,\@size) * 100;
my $cor_SE_size = corr(\@se,\@size) * 100;
my $cor_se = corr(\@lct,\@se) * 100;
say "corr LCT/nb_insts=$cor_LCT_insts%";
say "corr LCT/nb_blks=$cor_LCT_blks%";
say "corr LCT/nb_aseq_gis=$cor_LCT_agis%";
say "corr LCT/nb_aseq_his=$cor_LCT_ahis%";
say "corr LCT/nb_aseq_is=$cor_LCT_ais%";
say "corr LCT/size=$cor_LCT_size%";
say "corr LCT/SE=$cor_se%";
say "corr SE/nb_insts=$cor_SE_insts%";
say "corr SE/nb_blks=$cor_SE_blks%";
say "corr SE/nb_aseq_is=$cor_SE_ais%";
say "corr SE/size=$cor_SE_size%";
