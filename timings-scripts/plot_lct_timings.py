#!/usr/bin/python3

import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys

mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

##
# Reading data
##

if len(sys.argv) != 5:
  raise Exception("Four arguments are expected: the timings csv, the reference column, its legend, and the output PDF")
_, timings, ref_col, ref_legend, output_file = sys.argv

dfcoords = pd.read_csv(timings)
assert (isinstance(dfcoords, pd.DataFrame));

##
# Generating PDF
##

fig, ax = plt.subplots()

def do_plot(col, style: str, label: str, color: str):
  x = dfcoords[ref_col]
  y = dfcoords[col]
  plt.plot(x, y, style, label=label, color=color)

plt.xscale("log")
plt.yscale("log")
do_plot("SE", "x", "SE Validator", "green")
do_plot("LCT", "+", "LCT Oracle", "black")
ax.get_xaxis().set_major_formatter(
    mpl.ticker.FuncFormatter(lambda x, p: format(int(x))))
ax.get_yaxis().set_major_formatter(
    mpl.ticker.FuncFormatter(lambda x, p: format(float(x))))
diag_line, = ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3", label="slope of 1")

ax.set_ylabel("Average time of one run (s)")
ax.set_xlabel(ref_legend)
ax.legend()

plt.savefig(output_file)
